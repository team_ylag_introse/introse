<?php 
class QtrStats extends CI_Model {

	
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();

	}
	public function deleteWhereGameStatisticsNo($gameStatisticsNo){

		$data = array(
			'gameStatisticsNo' => $gameStatisticsNo
		);
		$this->db->delete('quarterstatistics', $data); 
	}
	public function insert($qtrStats)
	{
		$data = array(
			'gameStatisticsNo' => $qtrStats["gameStatisticsNo"],
			'teamNo' => $qtrStats["teamNo"]
		);
		$this->db->insert('quarterstatistics', $data); 
		return $this->db->insert_id();
	}
	public function get($id)
	{
		$this->db->select('*');
		$this->db->where('idQuarterStatistics', $id);
		$query = $this->db->get('quarterstatistics');
		return $query;
	}
	
	public function update($stats)
	{
		$data = array(
			'teamScore' => $stats["teamScore"]
		);
		$this->db->where('idquarterStatistics', $stats["idquarterStatistics"]);
		$this->db->update('quarterstatistics', $data);

	}

	public function validate_entry($qtstats) 
	{

		if($qtstats == "" || $qtstats < 0 ) 
		{
			return "Invalid Input! Please check highlighted fields.";
		}
		return "";
	}      

} 
?>