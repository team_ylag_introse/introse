<html>

	<head>
		<title>LSAL - Homepage</title>
		
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />


	</head>


	<body>
	<!-- Navbar for Public pages -->
			<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				        <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				   
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		      		
		    	</div>
		  	</div>
		</nav>
	<!-- end of .nav -->

	<!-- Page contents --> 


	<div class="container">
	    	<div class="page-header">
	    		<center>
	    			<br>
	    			<h1><?php echo $Teams[0]["teamName"]; ?></h1>
	    			<br>
	    			<h4> Wins: <?php echo $Teams[0]["win"]; ?>      Loses: <?php echo $Teams[0]["lose"]; ?> </h4>

	    		</center>
	    	</div>


	    	<div class="row">
		    	<div class="col-md-8  col-md-offset-2">

		    		<h3><span class="glyphicon glyphicon-list-alt"></span>&nbsp; &nbsp;Roster</h3>
		    		<br>
		    		<table class ="table table-striped table-hover">

			    		<col width="200px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">

		    			<thead style="background-color:#2c3e50; color:white;">
		    				<th>Player Name</th>
		    				<th>Number</th>
		    				<th>PPG</th>
		    				<th>RPG</th>
		    				<th>APG</th>
		    				<th>BPG</th>
		    				<th>SPG</th>
		    			</thead>

		    			<tbody>
		    				<?php $i=0; 
		    					foreach($Players as $player){ ?>
		    				<tr>
		    					<td><?php echo $player["firstName"]. " " .$player["lastName"];?></td>
		    					<td><?php echo $player["uniformNumber"];?></td>
		    					<td><?php echo $PerGame[$i][0]["PPG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["RPG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["APG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["BPG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["SPG"];?></td>
		    				</tr>
		    				<?php $i++;} ?>
		    				
		    			</tbody>
		    		</table>
		    	</div>

		    	
	    	</div>
	    	<div class="row">
	    	<div class="col-md-8  col-md-offset-2">
		    		<h3><span class="glyphicon glyphicon-calendar"></span>&nbsp; &nbsp;Schedule</h3>
		    		<br>

		    		<table class ="table table-striped table-hover">

		    			<col width="200px">
						<col width="200px">
		    			<col width="150px">


		    			<thead style="background-color:#2c3e50; color:white;">
		    				<th>vs.</th>
		    				<th>Date and Time</th>
		    				<th>Venue</th>
		    			</thead>

		    			<tbody>
		    				<?php  $i=0;?>
		    				<?php foreach ($Schedule as $schedule){ ?>
		    				<tr> 
		    					<td><a href="<?php echo base_url("home/viewGameStats")."/?gameNo=".$schedule["idGame"]; ?>"><?php  echo $Opponent[$i][0]["teamName"]; ?> </a></td>
		    					<td><?php  echo $schedule["date"]. "<br>" .$schedule["start"]. " - " .$schedule["end"]; ?></td>
		    					<td><?php  echo $schedule["court"]; ?></td>	
		    				</tr>
		    				<?php $i++; } ?>
		    			</tbody>
		    		</table>
		    	</div>
		    	</div>
	</div>





	</body>

	<script type="text/javascript" src="js/jquery.js"></script>


	<script type="text/javascript" src="js/bootstrap.js"></script>


</html>