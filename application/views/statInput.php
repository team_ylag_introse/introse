 	<!DOCTYPE html>
<html>
	<head>
		<title>LSAL - Homepage</title>
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jasny-bootstrap.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/adminpage.css');?>">



		

		<link rel="icon" type="image/ico" href="<?php echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/jasny-bootstrap.js');?>"></script>
		
		<script type="text/javascript" src="<?php echo base_url('resources/js/checkfield.js');?>"></script> 
	
		
		<meta charset="ISO-8859-1">
	
	</head>
	
	<body>
		

		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
					<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
			 				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<div class="row">
	    		<center>
	    			<h1> TEAM <?php echo $TeamOne["team"]['teamName'];?>  VS 
	    				 TEAM <?php  echo $TeamTwo["team"]['teamName'];?></h1>
	    			<h5> Elimination Round</h5>
	    			<h5>  <?php echo "Date: ".$gameDetail[0]['date']; ?></h5>
	    			<h5>  <?php echo "Time: ".$gameDetail[0]['start']. "-" .$gameDetail[0]['end']; ?></h5>
	    			<h5>  <?php echo "Court ".$gameDetail[0]['court']; ?></h5>
	    		
	    			<h1>Team Scores</h1>
	    			
	    			<?php 
					if (!empty($error))
					{
						echo 	"<div class='panel panel-danger'>
  									<div class='panel-heading'>
    									<h3 class='panel-title'><span class='glyphicon glyphicon-remove'></span>&nbsp;Error</h3>
  									</div>
  									<div class='panel-body'>";
    					echo $error;
  						echo 		"</div>
								</div>";
						
					}
					else if (!empty($update)) {
						echo "<div class='alert alert-dismissible alert-info'>
						  		<button type='button' class='close' data-dismiss='alert'>x</button>
						  		<strong>Data has been saved successfully!</strong> 
								</div>";}
				  ?>
	    			<br/>
	    			
	    		</center>
	    	
	    		<!-- Players Table --> 
					<div class="row">
						<table class="table table-bordered table-condensed table-striped table-hover table-register table-score" >
      						<thead>
      							<tr>
      								<th>Team</th>
      								<th>1</th>
      								<th>2</th>
      								<th>3</th>
      								<th>4</th>
      								<th>OT</th>
      								
      							</tr>

      						</thead>

      						<tbody >
								<tr class="score">
								
									<td>
										TEAM <?php  echo htmlentities($TeamOne["team"]['teamName']); ?> 
										 <form id="yoh" method ="POST"  action="<?php echo base_url('/admin/statsInput?gameNo=').$gameNo; ?>">
									</td>
									
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamOne["quarters"]["firstQuarter"]["teamScore"]); ?>" name="TeamOne[quarters][firstQuarter][teamScore]"/>
									</td>
									
									<td>
										<input type="number" min ="0"  class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamOne["quarters"]["secondQuarter"]["teamScore"]); ?>" name="TeamOne[quarters][secondQuarter][teamScore]"//>
									</td>
									
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamOne["quarters"]["thirdQuarter"]["teamScore"]); ?>" name="TeamOne[quarters][thirdQuarter][teamScore]"//>
									</td>
									
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamOne["quarters"]["fourthQuarter"]["teamScore"]); ?>" name="TeamOne[quarters][fourthQuarter][teamScore]"//>
									</td>
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Total" value="<?php echo htmlentities($TeamOne["quarters"]["overTimeQuarter"]["teamScore"]); ?>" name="TeamOne[quarters][overTimeQuarter][teamScore]"//>
									</td>
									
						         
						         
						         <tr class="score">
						         	<td>
										TEAM <?php  echo htmlentities($TeamTwo["team"]['teamName']);  ?>
									</td>
									
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamTwo["quarters"]["firstQuarter"]["teamScore"]); ?>" name="TeamTwo[quarters][firstQuarter][teamScore]"//>
									</td>
									
									<td>
										<input type="number"min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamTwo["quarters"]["secondQuarter"]["teamScore"]); ?>" name="TeamTwo[quarters][secondQuarter][teamScore]"//>
									</td>
									
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamTwo["quarters"]["thirdQuarter"]["teamScore"]); ?>" name="TeamTwo[quarters][thirdQuarter][teamScore]"//>
									</td>
									
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Score" value="<?php echo htmlentities($TeamTwo["quarters"]["fourthQuarter"]["teamScore"]); ?>" name="TeamTwo[quarters][fourthQuarter][teamScore]"//>
									</td>	
									<td>
										<input type="number" min ="0" class="form-control" placeholder="Total" value="<?php echo htmlentities($TeamTwo["quarters"]["overTimeQuarter"]["teamScore"]); ?>" name="TeamTwo[quarters][overTimeQuarter][teamScore]"//>
									</td>		 
								
						                 
      						</tbody>
    					</table>
    					<br>
    				
    					<center>
    						<h2> Player Statistics </h2>
    					</center>
    					
    					<ul class="nav nav-tabs">
							<li class="active"><a href="#A" data-toggle="tab" aria-expanded="true"><?php echo htmlentities($TeamOne["team"]['teamName']);?>  </a></li>
							<li class=""><a href="#B" data-toggle="tab" aria-expanded="true"><?php echo htmlentities($TeamTwo["team"]['teamName']); ?></a></li>
							<input type="hidden" min ="0" class="form-control" value="<?php echo $TeamOne["team"]['teamName']; ?>" name="TeamOne[team][teamName]" />
							<input type="hidden" min ="0" class="form-control" value="<?php echo $TeamTwo["team"]['teamName']; ?>" name="TeamTwo[team][teamName]" />
											
						</ul>
						
						<div id="myTabContent" class="tab-content">
						  <div class="tab-pane fade active in" id="A">
						    <table class="table table-bordered table-condensed table-striped table-hover table-register table-score">
    								<thead>
    									<tr>
		      								<th>Player</th>
		      								<th>FG 2 pts</th>
		      								<th>FG 3 pts</th>
		      								<th>Free Throw</th>
		      								<th>Rebound</th>
		      								<th>Assists</th>
		      								<th>Steal</th>
		      								<th>Blocks</th>
		      								<th>Fouls</th>
		      							</tr>
		    						</thead>
    						<?php 
	    								$i=0;
	    								foreach($TeamOne["players"] as $player) {


	    							?>
	    							<tbody>

	    								<td>
											<?php echo ($i +1).". ".htmlentities($player["firstName"])." ".htmlentities($player["lastName"]); ?>
											<input type="hidden" min ="0" class="form-control" value="<?php echo $player["firstName"]; ?>" name="TeamOne[players][<?php echo $i?>][firstName]" />
											<input type="hidden" min ="0" class="form-control" value="<?php echo $player["lastName"]; ?>" name="TeamOne[players][<?php echo $i?>][lastName]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["twoPoints"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][twoPoints]"  />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["threePoints"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][threePoints]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["freeThrows"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][freeThrows]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["rebounds"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][rebounds]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["assist"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][assist]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["steals"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][steals]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["blocks"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][blocks]" />
										</td>
										<td>
											<input style="width:100px;" type="number" min ="0" max = "6"  class="form-control" value="<?php echo htmlentities($player["playerStats"]["fouls"]); ?>" name="TeamOne[players][<?php echo $i; ?>][playerStats][fouls]" />
										</td>
										<?php 
										$i++; } ?>
	    							</tbody>
    					
    							</table>
						  </div>
						  <div class="tab-pane fade" id="B">
						    <table class="table table-bordered table-condensed table-striped table-hover table-register table-score">
    								<thead>
    									<tr>
		      								<th>Player</th>
		      								<th>FG 2 pts</th>
		      								<th>FG 3 pts</th>
		      								<th>Free Throw</th>
		      								<th>Rebound</th>
		      								<th>Assists</th>
		      								<th>Steal</th>
		      								<th>Blocks</th>
		      								<th>Fouls</th>
		      							</tr>
		    						</thead>
    						<?php 
	    								$i=0;
	    								foreach($TeamTwo["players"] as $player) {


	    							?>
	    								<tbody>

	    								
	    								<td>
											<?php echo ($i +1).". ".htmlentities($player["firstName"])." ".htmlentities($player["lastName"]); ?>
											<input type="hidden" min ="0" class="form-control" value="<?php echo $player["firstName"]; ?>" name="TeamTwo[players][<?php echo $i?>][firstName]" />
											<input type="hidden" min ="0" class="form-control" value="<?php echo $player["lastName"]; ?>" name="TeamTwo[players][<?php echo $i?>][lastName]" />
										
										</td>
										
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["twoPoints"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][twoPoints]" />
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["threePoints"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][threePoints]"/>
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["freeThrows"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][freeThrows]"/>
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["rebounds"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][rebounds]"/>
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["assist"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][assist]"/>
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["steals"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][steals]"/>
										</td>
										<td>
											<input type="number" min ="0" class="form-control" value="<?php echo htmlentities($player["playerStats"]["blocks"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][blocks]"/>
										</td>
										<td>
											<input style="width:100px;  " type="number" min ="0" max = "6" class="form-control" value="<?php echo htmlentities($player["playerStats"]["fouls"]); ?>" name="TeamTwo[players][<?php echo $i; ?>][playerStats][fouls]"/>
										</td>
										<?php 
										$i++;
									} ?>
	    							</tbody>
    					
    							</table>
						  </div>
						</div>
				
					<!-- End of Players table-->

					<br> <br> <br>
					<div  class="row">
						<center>
							

							<div class="form-group">
								<br>
								<input type="submit" id="submitButton" class="btn btn-success btn-lg" name="submit" value="submit">
								<input type="hidden" name="gameNo" value="<?php echo $gameNo; ?>" />
							</div>

							</form>
						</center>

					</div>
	    		
	    		</div>
	    	</div>
	    </div>
	    
	
 
	</body>



</html>