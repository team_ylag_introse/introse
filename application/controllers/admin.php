<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Bracket', 'bracketModel');
		$this->load->model('Team', 'teamModel');
		$this->load->model('Account', 'accountModel');
		$this->load->model("Game",'gameModel');
		$this->load->model('Statis', 'StatModel');
		$this->load->model('QtrStats', 'QtrModel');
		$this->load->model('Player', 'playerModel');
		$this->load->model('PlayerStats', 'playerStatModel');
		$this->load->model('GameStatistics', 'gameStatisticsModel');

	}

	public function index()
	{
		if($this->input->post() != NULL)
		{
			$user = $this->input->post("username");
			$pass = $this->input->post("password");

		

			$account= $this->accountModel->get($user,$pass)->result_array()[0];

			if($account == NULL)
			{
				$data['error'] = "Invalid input.";
			}
			else 
			{
				$_SESSION['username'] = $user; 
				$_SESSION['password'] = $pass;
				?>
					<script> self.location = "<?php echo base_url('/admin/home'); ?>";</script>
				<?php
				exit();
			}
		}
		$this->load->view('login', $data);
	}

	public function home()
	{
		$this->load->view('admin_home');
	}
	public function viewBracket()
	{
		if($this->bracketModel->get()->result_array() == NULL)
		{
			$teams = $this->teamModel->getRand()->result_array();
			
			$bracketCtr = 0;
			foreach ($teams as $team) 
			{	
				$bracket["win"] = $team["win"];
				$bracket["lose"] = $team["lose"];
				$bracket["team"] = $team["idTeam"];
				$bracket["bracket"] = $bracketCtr%8 + 1;
				$bracketCtr++;
				$this->bracketModel->insert($bracket);
			}
		}

		
		for ($bracketCtr=0; $bracketCtr < 8; $bracketCtr++) { 
			$data["bracket"][$bracketCtr] = $this->bracketModel->getBracket($bracketCtr+1)->result_array();
		}
		
		$data["bracket"]["BracketTeams"] = $this->bracketModel->countBracket()->result_array();
		$data["bracket"]["TotalTeams"] =	$this->bracketModel->countTeam()->result_array();
		$this->load->view('viewbracket', $data);
	}
	public function randBracket()
	{
		?> 
			<script>
				var r = confirm("Do you want to change the bracket?");
				if(r == true)
				{
					<?php 
					$this->bracketModel->deleteAll();
					?>
				}
				self.location = "<?php echo base_url("admin/viewBracket"); ?>"
			</script>
		<?php 
	}


	public function addMatch(){

		if($this->input->get("date") != "")
		{
			$date = $this->input->get("date");
		}
		else if($this->input->post("date") != "")
		{
			$date = $this->input->post("date");
		}

		else
		{	
			$date = date("Y-m-d", time() + 86400);
		}


		$data["date"] = $date;


		$data["game"] = $this->gameModel->getWhereDate($date)->result_array();

		if(($this->input->post("submit")))
		{	
			$ctr = 0;
			$ctr2 = 8;

			if($data["date"] < date("Y-m-d", time() + 86400)){
				$data["error"] = "Error found in Date. Entered Date has already passed. Unable to create matches.";
			}
			foreach ($this->input->post("game") as $game) 
			{
				
				if ($game["teamA"] != "------------" && $game["teamB"] == "------------") {

					$data["error"] = "Error found in Court " .strtoupper($game["court"]). " at " .$game["start"]. " - " .$game["end"]. ". No team selected for Team B."; 
					break;
				}
				if($game["teamA"] == "------------" && $game["teamB"] != "------------") {

					$data["error"] = "Error found in Court " .strtoupper($game["court"]). " at " .$game["start"]. " - " .$game["end"]. ". No team selected for Team A."; 
					break;
				}
				if($game["teamA"] == $game["teamB"] && !(($game["teamA"] == "------------" && $game["teamB"] == "------------")))
				{

					$data["error"] = "Error found in Court " .strtoupper($game["court"]). " at " .$game["start"]. " - " .$game["end"]. ". Teams cannot have a match with themselves."; 
					break;
				}

				if(($this->input->post("game")[$ctr]["teamA"] == $this->input->post("game")[$ctr2]["teamA"] || ($this->input->post("game")[$ctr]["teamB"] == $this->input->post("game")[$ctr2]["teamB"] || ($this->input->post("game")[$ctr]["teamA"] == $this->input->post("game")[$ctr2]["teamB"] || ($this->input->post("game")[$ctr]["teamB"] == $this->input->post("game")[$ctr2]["teamA"])))) && !(($game["teamA"] == "------------" && $game["teamB"] == "------------")) )
				{

					$data["error"] = "Error found in Court B at ".$this->input->post("game")[$ctr]["start"]. " - " .$this->input->post("game")[$ctr]["end"]. ". One of the teams already has a scheduled game."; 
					break;
				}
				$ctr++;$ctr2++;

			
			}
						
				
			
			if($data["error"] == "")
			{
				if($this->gameModel->getWhereDate($date)->result_array()[0] != "")
				{	
					$idGames = $this->gameModel->getWhereDate($date)->result_array();

					foreach ($this->input->post("game") as $game) 
					{

						$game["date"] = $date;
						$this->gameModel->update($game);
					}
				}
				else if($this->gameModel->getWhereDate($date)->result_array()[0] == "")
				{
					foreach ($this->input->post("game") as $game) 
					{
						$game["date"] = $date;
						$gameId = $this->gameModel->insert($game);
					}
				}
				$data["updated"] = "success";
			}
			$data["game"] = $this->input->post("game");
		}

			
		
		$data["teams"] = $this->teamModel->getAll()->result_array();
		$this->load->view('SchedMaking', $data); 

	}
	public function test(){

		print_r(date("Y-m-d"));

		if("2016-04-30" < "2016-04-29")
		{
			echo "fdhszgbjknlm";
		}
	}
	public function statsInput()
	{	

		$data = $this->input->post();

		$gameNo = $this->input->get("gameNo");
		if(!is_numeric($gameNo) || $gameNo == "")
		{
			?>
				<script> self.location = "<?php echo base_url('/admin/viewMatch'); ?>";</script>
			<?php
			exit();
		}

		if($this->gameModel->get($gameNo)->num_rows() == 0){
			?>
			<script> self.location = "<?php echo base_url('/admin/viewMatch'); ?>";</script>
			<?php
			exit();
		}
 //MUST SA FRIDAY
	/*	if($this->gameModel->get($gameNo)->result_array[0]["date"] < date("Y-m-d"))
		{
			?>

			<script>
				alert("You cannot add statistics right now."); 
					self.location = "<?php echo base_url('/admin/viewMatch'); ?>";
					</script>
			<?php
			exit();
	
		} */


	

		$game = $this->gameModel->get($gameNo)->result_array();
		$gameId = $game[0]["idGame"];
		$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
		$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
		
		if($this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->num_rows() == 0){
			$data["gameStatistics"][0]["gameNo"] = $gameId;
			$data["gameStatistics"][0]["idTeam"] = $game[0]["teamA"];


			$data["gameStatistics"][1]["gameNo"] = $gameId;
			$data["gameStatistics"][1]["idTeam"] = $game[0]["teamB"];

			$data["qtr"]["gameStatisticsNo"] = $this->gameStatisticsModel->insert($data["gameStatistics"][0]);
			$data["qtr"]["teamNo"] = $game[0]["teamA"];



			$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
			foreach ($data["TeamOne"]["players"] as $player) {
				$this->playerStatModel->insert($player["idPlayer"],$gameId);
			}



			$gameStatistics[0]["id"]  = $data["qtr"]["gameStatisticsNo"];
			$gameStatistics[0]["qtr1"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[0]["qtr2"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[0]["qtr3"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[0]["qtr4"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[0]["ot"]  = $this->QtrModel->insert($data["qtr"]);

			$this->gameStatisticsModel->update($gameStatistics[0]);


			$data["qtr"]["gameStatisticsNo"] = $this->gameStatisticsModel->insert($data["gameStatistics"][1]);
			$data["qtr"]["teamNo"] = $game[0]["teamB"];
			
			$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();

			foreach ($data["TeamTwo"]["players"] as $player) {
				$this->playerStatModel->insert($player["idPlayer"],$gameId);
			}


			$gameStatistics[1]["id"]  = $data["qtr"]["gameStatisticsNo"];
			$gameStatistics[1]["qtr1"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[1]["qtr2"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[1]["qtr3"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[1]["qtr4"] = $this->QtrModel->insert($data["qtr"]);
			$gameStatistics[1]["ot"]  = $this->QtrModel->insert($data["qtr"]);

			$this->gameStatisticsModel->update($gameStatistics[1]);

			$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
			$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
		

		}
		
		if($this->input->post("submit") != "")
		{ 
			for ($i=0; $i < count($data["TeamOne"]["players"]); $i++) { 
				$teamOne["playerStats"][$i] = $data["TeamOne"]["players"][$i]["playerStats"];

			}
			for ($i=0; $i < count($data["TeamTwo"]["players"]); $i++) { 
				$teamTwo["playerStats"][$i] = $data["TeamTwo"]["players"][$i]["playerStats"];

			}

			$data["TeamOne"]["team"] = $this->teamModel->get($game[0]["teamA"])->result_array()[0];
			$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
			$data["TeamTwo"]["team"] = $this->teamModel->get($game[0]["teamB"])->result_array()[0];
			$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();
			


			for ($i=0; $i < count($data["TeamOne"]["players"]); $i++) { 
				$data["TeamOne"]["players"][$i]["playerStats"] = $teamOne["playerStats"][$i];

			}
			for ($i=0; $i < count($data["TeamTwo"]["players"]); $i++) { 
				$data["TeamTwo"]["players"][$i]["playerStats"] = $teamTwo["playerStats"][$i];

			}



			$i = 0;
			foreach ($data["TeamOne"]["quarters"] as $quarter) {
				$teamOne["quarters"][$i] = $quarter["teamScore"];
				$i++;
			}

			$i = 0;
			foreach ($data["TeamTwo"]["quarters"] as $quarter) {
				$teamTwo["quarters"][$i] = $quarter["teamScore"];
				$i++;
			}

			$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
			$data["TeamOne"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];




			$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
			$data["TeamTwo"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];

			

			$data["TeamOne"]["quarters"]["firstQuarter"]["teamScore"] = $teamOne["quarters"][0];
			$data["TeamOne"]["quarters"]["secondQuarter"]["teamScore"] = $teamOne["quarters"][1];
			$data["TeamOne"]["quarters"]["thirdQuarter"]["teamScore"] = $teamOne["quarters"][2];
			$data["TeamOne"]["quarters"]["fourthQuarter"]["teamScore"] = $teamOne["quarters"][3];
			$data["TeamOne"]["quarters"]["overTimeQuarter"]["teamScore"] = $teamOne["quarters"][4];



			$data["TeamTwo"]["quarters"]["firstQuarter"]["teamScore"] = $teamTwo["quarters"][0];
			$data["TeamTwo"]["quarters"]["secondQuarter"]["teamScore"] = $teamTwo["quarters"][1];
			$data["TeamTwo"]["quarters"]["thirdQuarter"]["teamScore"] = $teamTwo["quarters"][2];
			$data["TeamTwo"]["quarters"]["fourthQuarter"]["teamScore"] = $teamTwo["quarters"][3];
			$data["TeamTwo"]["quarters"]["overTimeQuarter"]["teamScore"] = $teamTwo["quarters"][4];

			
			$data["error"] = "";
			$QTRTOT1 = 0;
			$QTRTOT2 = 0;
			$$GTOTAL = 0;
			$$GTOTAL2 = 0;

			foreach ($data["TeamOne"]["quarters"] as $quarterStat) 
			{
				$data["error"] = $this->QtrModel->validate_entry($quarterStat["teamScore"]);
				if($data["error"] != "")
				{
					break;
				}else{
					$QTRTOT1 += $quarterStat["teamScore"];
				}
			}
			if($data["error"] == "")
			{
				foreach ($data["TeamTwo"]["quarters"] as $quarterStat) {
					$data["error"] = $this->QtrModel->validate_entry($quarterStat["teamScore"]);
					if($data["error"] != "")
					{
						break;
					}else{
						$QTRTOT2 += $quarterStat["teamScore"];
					}
				}
			}


			if($data["error"] == "")
			{
				foreach ($data["TeamOne"]["players"] as $player) 
				{
					$data["error"] = $this->StatModel->validate_entry($player["playerStats"]);
					if($data["error"] != ""){
						break;
					}else {
						$Total = 2 * $player["playerStats"]["twoPoints"] + 3 * $player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];
					}
					$GTOTAL += $Total;
				}
			}


			if($data["error"] == ""){
				foreach ($data["TeamTwo"]["players"] as $player) 
				{
					$data["error"] = $this->StatModel->validate_entry($player["playerStats"]);
					if($data["error"] != ""){
						break;
					}else {
						$Total2 = 2 * $player["playerStats"]["twoPoints"] + 3 * $player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];
					
					}
					$ctr++;
					$GTOTAL2 += $Total2;

				}
			}

			if($data["error"] == "")
			{
				if ($GTOTAL != $QTRTOT1 || $GTOTAL2 != $QTRTOT2 )
				{
					$data["error"] = "Point total does not match.";

				}
			}

			if($data["error"] == "")
			{
				if ($GTOTAL == $GTOTAL2 )
				{
					$data["error"] = "Both final scores match. Unable to determine winner. ";

				}
			}


			if($data["error"] == "")
			{

				foreach ($data["TeamOne"]["players"] as $player) 
				{
					$player["playerStats"]["totalPoints"] = 2 * $player["playerStats"]["twoPoints"] + 3*$player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];

					$player["playerStats"]["playerID"] = $player["idPlayer"];
					$player["playerStats"]["gameNo"] = $gameNo;

					$this->playerStatModel->update($player["playerStats"]); 
				}




				foreach ($data["TeamTwo"]["players"] as $player)  
				{

					$player["playerStats"]["totalPoints"] = 2 * $player["playerStats"]["twoPoints"] + 3* $player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];

					$player["playerStats"]["playerID"] = $player["idPlayer"];
					$player["playerStats"]["gameNo"] = $gameNo;

					$this->playerStatModel->update($player["playerStats"]); 

				}


				foreach ($data["TeamOne"]["quarters"] as $quarter) {
					$this->QtrModel->update($quarter); 	 

				}



				foreach ($data["TeamTwo"]["quarters"] as $quarter) 
				{
					$this->QtrModel->update($quarter); 		

				}


				if($GTOTAL > $GTOTAL2){
					$this->teamModel->updateWin($game[0]["teamA"],$data["TeamOne"]["team"]["win"]+1);
					$this->teamModel->updateLose($game[0]["teamB"],$data["TeamTwo"]["team"]["lose"]+1);}
				else{
					$this->teamModel->updateWin($game[0]["teamB"],$data["TeamTwo"]["team"]["win"]+1);
					$this->teamModel->updateLose($game[0]["teamA"],$data["TeamOne"]["team"]["lose"]+1);}  


				$data["update"] = "success";
			}
		}
		else
		{
			$data["TeamOne"]["team"] = $this->teamModel->get($game[0]["teamA"])->result_array()[0];
			$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
			
			$i = 0;

			foreach ($data["TeamOne"]["players"] as $player) 
			{
				$data["TeamOne"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
				$i++;
			}

			$data["TeamOne"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];

			$data["TeamTwo"]["team"] = $this->teamModel->get($game[0]["teamB"])->result_array()[0];
			$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();
			$i = 0;
			foreach ($data["TeamTwo"]["players"] as $player) 
			{
				$data["TeamTwo"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
				$i++;
			}
			$data["TeamTwo"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];
			$data["gameNo"] = $gameNo;
		}
		
		$data["gameDetail"] = $game; 
		$this->load->view('statInput', $data);

	}
	public function statsView(){
		$gameNo = $this->input->get("gameNo");
		if(!is_numeric($gameNo) || $gameNo == "")
		{
			?>
				<script> self.location = "<?php echo base_url('/admin/viewMatch'); ?>";</script>
			<?php
			exit();
		}

		

		/*$query = $this->db->query('SELECT DISTINCT `game`.`idGame`, `idPlayer`, `teamname`, concat(`firstname`," ", `lastname`) as `Names`, `team`.`idTeam` , `uniformNumber` 
								   FROM `lssc`.`player`, `lssc`.`team`,`lssc`.`game` 
								   WHERE `game`.`idGame` = '.$gameNo.' 
								   AND `game`.`teamA` = `team`.`idTeam` 
								   AND `team`.`idTeam` = `player`.`teamNo`;');

		$data["TeamOne"] = $query->result_array()[0];
		$_SESSION["TeamOne"] = $data["TeamOne"];


		$query = $this->db->query('SELECT DISTINCT `game`.`idGame`, `idPlayer`, `teamname`, concat(`firstname`," ", `lastname`) as `Names`, `team`.`idTeam` , `uniformNumber` 
								   FROM `lssc`.`player`, `lssc`.`team`,`lssc`.`game` 
								   WHERE `game`.`idGame` = '.$gameNo.' 
								   AND `game`.`teamB` = `team`.`idTeam` 
								   AND `team`.`idTeam` = `player`.`teamNo`;');

		$data["TeamTwo"] = $query->result_array()[0];
		$_SESSION["TeamTwo"] = $data["TeamTwo"];
		*/

		$game = $this->gameModel->get($gameNo)->result_array();

		$data["TeamOne"]["team"] = $this->teamModel->get($game[0]["teamA"])->result_array()[0];
		$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
		$i = 0;
		foreach ($data["TeamOne"]["players"] as $player) 
		{
			$data["TeamOne"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
			$i++;
		}

		$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
		$data["TeamOne"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];

		$data["TeamTwo"]["team"] = $this->teamModel->get($game[0]["teamB"])->result_array()[0];
		$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();
		$i = 0;
		foreach ($data["TeamTwo"]["players"] as $player) 
		{
			$data["TeamTwo"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
			$i++;
		}
		$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
		$data["TeamTwo"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];
		$data["gameNo"] = $gameNo;
		$data["gameDetail"] = $game; 
		
		$this->load->view('VIEW', $data);
	}
	public function viewMatch()
	{
		if($this->input->get("date") != "")
		{
			$date = $this->input->get("date");
		}
		else if($this->input->post("date") != "")
		{
			$date = $this->input->post("date");
		}
		else
		{
			$date = date("Y-m-d", time() + 86400);
		}

		
/*
		$query = $this->db->query(
			'SELECT distinct `game`.`idGame`,
							 `T1firstQuarter`.`teamScore` AS T1firstQuarter,
							 `T1secondQuarter`.`teamScore` AS T1secondQuarter,
							 `T1thirdQuarter`.`teamScore` AS T1thirdQuarter,
							 `T1fourthQuarter`.`teamScore` AS T1fourthQuarter, 
							 `T1overTime`.`teamScore` AS T1overTime, 
							 `T2firstQuarter`.`teamScore` AS T2firstQuarter,
							 `T2secondQuarter`.`teamScore` AS T2secondQuarter,
							 `T2thirdQuarter`.`teamScore` AS T2thirdQuarter,
							 `T2fourthQuarter`.`teamScore` AS T2fourthQuarter, 
							 `T2overTime`.`teamScore` AS T2overTime, 
							 `teamA`.`teamName` AS `teamA`, 
							 `teamB`.`teamName` AS `teamB`, 
							 `game`.`date` as `Date`,
							 `game`.`start`, 
							 `game`.`end`, 
							 `court` 
			FROM `game`, 
				 `team` AS `teamA`, 
				 `team` AS `teamB`, 
				 `gamestatistics` AS gst1, 
				 `gamestatistics` AS gst2,
				 `quarterstatistics` AS `T1firstQuarter`,
				 `quarterstatistics` AS `T1secondQuarter`,
				 `quarterstatistics` AS `T1thirdQuarter`,
				 `quarterstatistics` AS `T1fourthQuarter`,
				 `quarterstatistics` AS `T1overTime`,
				 `quarterstatistics` AS `T2firstQuarter`,
				 `quarterstatistics` AS `T2secondQuarter`,
				 `quarterstatistics` AS `T2thirdQuarter`,
				 `quarterstatistics` AS `T2fourthQuarter`,
				 `quarterstatistics` AS `T2overTime`
			WHERE `teamA`.`idTeam` = `game`.`teamA` && 
				  `teamB`.`idTeam` = `game`.`teamB` && 
				  `T1firstQuarter`.`idquarterStatistics` = `gst1`.`firstQuarterNo` && 
				  `T1secondQuarter`.`idquarterStatistics` = `gst1`.`secondQuarterNo` && 
				  `T1thirdQuarter`.`idquarterStatistics` = `gst1`.`thirdQuarterNo` && 
				  `T1fourthQuarter`.`idquarterStatistics` = `gst1`.`fourthQuarterNo`&& 
				  `T1overTime`.`idquarterStatistics` = `gst1`.`overTimeQuarterNo` &&
				  `T2firstQuarter`.`idquarterStatistics` = `gst2`.`firstQuarterNo` && 
				  `T2secondQuarter`.`idquarterStatistics` = `gst2`.`secondQuarterNo` && 
				  `T2thirdQuarter`.`idquarterStatistics` = `gst2`.`thirdQuarterNo` && 
				  `T2fourthQuarter`.`idquarterStatistics` = `gst2`.`fourthQuarterNo`&& 
				  `T2overTime`.`idquarterStatistics` = `gst2`.`overTimeQuarterNo` &&
				  `game`.`date` = ?
				  ', $date);
				  
		$data["games"] = $query->result_array();
*/




		$data["date"] = $date;
		$games = $this->gameModel->getWhereDate($date)->result_array();
		$i = 0;
		foreach ($games as $game) 
		{
			$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($game["idGame"], $game["teamA"])->result_array();
			$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($game["idGame"], $game["teamB"])->result_array();
			

			$data["games"][$i]["details"] = $game;
			
			$data["games"][$i]["teamA"]["teamName"] = $this->teamModel->get($game["teamA"])->result_array()[0]["teamName"];
			$data["games"][$i]["teamA"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];


			$data["games"][$i]["teamB"]["teamName"] = $this->teamModel->get($game["teamB"])->result_array()[0]["teamName"];

			$data["games"][$i]["teamB"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];
			$i++;	
		}
			
		$this->load->view('admin-viewsched', $data);
	
	}

	public function viewTeams(){

		$data["Teams"] = $this->teamModel->getAll()->result_array();
		
		$this->load->view('admin-viewTeams',$data);

	}

	public function viewTeamPage()
	{
		$idTeam = $this->input->get("idTeam");
		if(!is_numeric($idTeam)){
			?>
				<script> self.location = "<?php echo base_url('home/viewBracket'); ?>";</script>
			<?php
			exit();
		}

		if($this->teamModel->get($idTeam)->num_rows() == 0){
			?>
			<script> self.location = "<?php echo base_url('home/viewBracket'); ?>";</script>
			<?php
			exit();
		}
		
		 
		$data["Teams"]= $this->teamModel->get($idTeam)->result_array();
		$data["Players"] = $this->playerModel->get_players_at_teamID($idTeam)->result_array();
		$data["Schedule"] = $this->teamModel->getSchedule($idTeam)->result_array();

		$date = date("Y-m-d", time() + 86400);
		$j=0;
		 foreach ($data["Players"] as $PerGame){
			$data["PerGame"][$j] = $this->playerStatModel->getStats($PerGame["idPlayer"],$date)->result_array();
			
			if($data["PerGame"][0][0]["PPG"] == NULL){
			$data["PerGame"][$j][0]["PPG"] = 0;
			$data["PerGame"][$j][0]["RPG"] = 0;
			$data["PerGame"][$j][0]["APG"] = 0;
			$data["PerGame"][$j][0]["BPG"] = 0;
			$data["PerGame"][$j][0]["SPG"] = 0;
		}
			$j++;
			} 


		$i=0;

		foreach($data["Schedule"] as $Opp){
		if($Opp["teamA"] == $idTeam){
			$data["Opponent"][$i] = $this->teamModel->get($Opp["teamB"])->result_array();
		}
		else if ($Opp["teamB"] == $idTeam){
			$data["Opponent"][$i] = $this->teamModel->get($Opp["teamA"])->result_array();
		}
		$i++;}
		
		$this->load->view('admin-viewTeamPage',$data);
		
	}

}
