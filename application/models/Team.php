<?php 
class Team extends CI_Model {


	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();

	}
	public function insert($team)
	{
		$data = array(
			'teamName' => $team["teamName"]
		);
		$this->db->insert('team', $data); 
		return $this->db->insert_id();
	}
	public function check($teamName)
	{
		$sql = "SELECT * FROM `team` WHERE UPPER(`teamName`) = ". strtoupper($this->db->escape($teamName));
		$query = $this->db->query($sql);
		if($query->num_rows() == 0){
			return FALSE;
		}
		return TRUE;
	}    

	public function updateWin($team,$win){
		
  		$data = array(
               'win' => $win
            );

		$this->db->where('idTeam', $team);
		$this->db->update('team', $data);

	}
	public function updateLose($team,$lose){
		
		$data = array(
               'lose' => $lose
            );

		$this->db->where('idTeam', $team);
		$this->db->update('team', $data);

	}
	public function get($id){
		$this->db->select('*');
		$this->db->where('idTeam', $id);
		$query = $this->db->get('team');
		return $query;
	}

	public function getSchedule($id){
		$this->db->select('*');
		$this->db->where('teamA', $id);
		$this->db->or_where('teamB', $id);
		$this->db->order_by("date", "asc");
		$this->db->order_by("start", "asc");
		$query = $this->db->get('game');
		return $query;

	}
	public function getAll(){

		$this->db->select('*');
		$query = $this->db->get('team');
		return $query;
	}
	public function getRand(){
		return $this->db->query('SELECT * FROM `team` ORDER BY RAND()');
	}

} 
?>