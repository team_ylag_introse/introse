<html>
	<head>
		<title>LSAL - View Bracket</title>
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/theme.css'); ?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jasny-bootstrap.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/adminpage.css'); ?>">
		<link rel="icon" type="image/ico" href="<?php echo base_url('resources/src/logo.png'); ?>" />


		<!-- View Sched CSS -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/viewsched.css');?>">

		<!-- JQUERY UI -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jquery-ui.css');?>">	
		<script type="text/javascript" src="<?php echo base_url('resources/js/jquery.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('resources/js/bootstrap.js'); ?>"></script>
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery-ui.js')?>"></script>
		<script type="text/javascript" src="<?php echo base_url('resources/js/jasny-bootstrap.js'); ?>"></script>
		<script type="text/javascript" src="<?php  echo base_url('resources/js/dateexclude.js')?>"></script>

	</head>


	<body>
		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
				<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
				
				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<h1>Brackets</h1>
				<div class = "row">
				    	<table class = "table">
				   			<thead>
				   				<th>Total Registered Teams: <?php echo $bracket["TotalTeams"][0]["count(*)"] ?> </th>
				   				<th>Total Teams in Brackets: <?php echo $bracket["BracketTeams"][0]["count(*)"] ?> </th>
				   			</thead>
				    			
				    	</table>
				</div>
	    	</div>

	    	<div class="row">
	    		<table class = "table table-striped">



	    			<thead>

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">
	    				
	    				<th bgcolor="#FF6347"><bold>Bracket 1</bold></th>
	    				<th bgcolor="#FF6347"><b>W</b></th>
	    				<th bgcolor="#FF6347"><b>L</b></th>


	    				<th bgcolor="#00BFFF"><bold>Bracket 2</bold></th>
	    				<th bgcolor="#00BFFF"><b>W</b></th>
	    				<th bgcolor="#00BFFF"><b>L</b></th>

	    				<th bgcolor="yellow"><bold>Bracket 3</bold></th>
	    				<th bgcolor="yellow"><b>W</b></th>
	    				<th bgcolor="yellow"><b>L</b></th>

	    				<th bgcolor="#90EE90"><bold>Bracket 4</bold></th>
	    				<th bgcolor="#90EE90"><b>W</b></th>
	    				<th bgcolor="#90EE90"><b>L</b></th>
	    			</thead>


	    			<tbody>

	    			<?php 
	    				for ($ctr=0; $ctr < count($bracket[0]); $ctr++) { 
	    				# code...
	    			?>
	    				<tr>
	    				<?php for ($ctr2=0; $ctr2 < 4; $ctr2++) { 
	    					# code...
	    				 ?>	
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["teamName"]); ?></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["win"]); ?></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["lose"]); ?></td>
	    					<?php } ?>
	    				</tr>
					<?php } ?>
	    				
	    			</tbody>
	    		</table>
	    	</div>

	    	<div class="row">
	    		<table class = "table table-striped">
	    			<thead>

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">
	    				
	    				<th bgcolor="pink"><bold>Bracket 5</bold></th>
	    				<th bgcolor="pink"><b>W</b></th>
	    				<th bgcolor="pink"><b>L</b></th>
	    				
	    				<th bgcolor="orange"><bold>Bracket 6</bold></th>
	    				<th bgcolor="orange"><b>W</b></th>
	    				<th bgcolor="orange"><b>L</b></th>

	    				<th bgcolor="#F5DEB3"><bold>Bracket 7</bold></th>
	    				<th bgcolor="#F5DEB3"><b>W</b></th>
	    				<th bgcolor="#F5DEB3"><b>L</b></th>

	    				<th bgcolor="violet"><bold>Bracket 8</bold></th>
	    				<th bgcolor="violet"><b>W</b></th>
	    				<th bgcolor="violet"><b>L</b></th>
	    			</thead>

	    			<tbody>
	    					<?php 
	    				for ($ctr=0; $ctr < count($bracket[0]); $ctr++) { 
	    				# code...
	    			?>
	    				<tr>
	    				<?php for ($ctr2=4; $ctr2 < 8; $ctr2++) { 
	    					# code...
	    				 ?>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["teamName"]); ?></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["win"]); ?></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["lose"]); ?></td>
	    					<?php } ?>
	    				</tr>
					<?php } ?>
	    				
	    			</tbody>
	    		</table>
	    	
				<center><a class="btn btn-default" href="<?php echo base_url("admin/randbracket"); ?>"> RECREATE BRACKET </a></center>
	    	</div>
	    </div>
	</body>

</html>