<?php 
class Player extends CI_Model {

	private $lastname;
	private $firstname;
	private $idno;
	private $contactNo;
	private $uniformNo;
	private $uniformSize;

	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();

	}

	public function insert($player)
	{
		$data = array(
			'lastName' => $player["lastName"],
			'middleName' => $player["middleName"],
			'firstName' => $player["firstName"],
			'idNo' => $player["idNo"],
			'uniformSize' => $player["uniformSize"],
			'contactNumber' => $player["contactNo"],
			'uniformNumber' => $player["uniformNo"],
			'teamNo' => '3'
		);
		$this->db->insert('player', $data); 
		return $this->db->insert_id();
	}
	

	public function get_players_at_teamID($id){
		$this->db->select('*');
		$this->db->where('teamNo', $id);
		$query = $this->db->get('player');
		return $query;
	}


	public function validate_entry($player){

		if($player["lastName"] == ""){
			return "Missing Last Name";
		}
		else if(!ctype_alpha($player["lastName"])){
			return "Invalid Last Name";
		}
		else if($player["firstName"] == ""){
			return "Missing First Name";
		}
		else if(!ctype_alpha($player["firstName"])){
			return "Invalid First Name";
		}
		else if($player["middleName"] == ""){
			return "Missing Middle Name";
		}
		else if(!ctype_alpha($player["middleName"])){
			return "Invalid Middle Name";
		}
		else if($player["idNo"] == ""){
			return "Missing ID Number";
		}else if(!is_numeric($player["idNo"])  || $player["idNo"] <= 0){
			return "Invalid ID Number";
		}
		else if($player["contactNo"] == ""){
			return "Missing Contact Number";
		}else if(!is_numeric($player["contactNo"]) || $player["contactNo"] <= 0){
			return "Invalid Contact Number";
		}
		else if($player["uniformNo"] == ""){
			return "Missing Uniform Number";
		}
		else if($player["uniformNo"] < 1 || $player["uniformNo"] > 99 ){
			return "Invalid Uniform Number";
		}else if(!is_numeric($player["uniformNo"])){
			return "Uniform Number Must Be Numeric";
		}else if($player["uniformSize"] == ""){
			return "Missing Uniform Size";
		}else if($player["uniformSize"] != "XS" && $player["uniformSize"] != "S" && $player["uniformSize"] != "M" && $player["uniformSize"] != "L" && $player["uniformSize"] != "XL" && $player["uniformSize"] != "XXL"){
			return "Missing Uniform Siz!!e";
		}
		return "";
	} 
	public function update_teamid($teamid, $playerid){
		$data = array(
               'teamNo' => $teamid
            );

		$this->db->where('idPlayer', $playerid);
		$this->db->update('player', $data);
	}    

} 
?>