<?php
@session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class addMatches extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();

        $this->load->model('Game', 'gameModel');
        $this->load->model('GameStatistics', 'gameStatisticsModel');
        $this->load->model('PlayerStats', 'playerStatModel');
        $this->load->model('Player', 'playerModel');
        $this->load->model('QtrStats', 'qtrStatsModel');
	}
	public function index()
	{
		$this->addmatch();
	}

	public function addmatch(){


		
	
		if($this->input->post("mymatch") != "")
		{
			$data = $this->input->post();
			

			$thematch = $this->input->post("mymatch");

			$data["game"]["teamA"] = $thematch["teamA"];
			$data["game"]["teamB"] = $thematch["teamB"];
			$data["game"]["dateTime"] = $thematch["dateTime"];
			$gameId = $this->gameModel->insert($data["game"]);





			$data["gameStatistics"][0]["gameNo"] = $gameId;
			$data["gameStatistics"][0]["idTeam"] = $thematch["teamA"];



			$data["gameStatistics"][1]["gameNo"] = $gameId;
			$data["gameStatistics"][1]["idTeam"] = $thematch["teamB"];

			$data["qtr"]["gameStatisticsNo"] = $this->gameStatisticsModel->insert($data["gameStatistics"][0]);
			$data["qtr"]["teamNo"] = $data["game"]["teamA"];

			$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($data["game"]["teamA"])->result_array();
			foreach ($data["TeamOne"]["players"] as $player) {
				$this->playerStatModel->insert($player["idPlayer"],$gameId);
			}



			$gameStatistics[0]["id"]  = $data["qtr"]["gameStatisticsNo"];
			$gameStatistics[0]["qtr1"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[0]["qtr2"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[0]["qtr3"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[0]["qtr4"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[0]["ot"]  = $this->qtrStatsModel->insert($data["qtr"]);

			$this->gameStatisticsModel->update($gameStatistics[0]);


			$data["qtr"]["gameStatisticsNo"] = $this->gameStatisticsModel->insert($data["gameStatistics"][1]);
			$data["qtr"]["teamNo"] = $data["game"]["teamB"];
			
			$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($data["game"]["teamB"])->result_array();
			foreach ($data["TeamTwo"]["players"] as $player) {
				$this->playerStatModel->insert($player["idPlayer"],$gameId);
			}


			$gameStatistics[1]["id"]  = $data["qtr"]["gameStatisticsNo"];
			$gameStatistics[1]["qtr1"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[1]["qtr2"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[1]["qtr3"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[1]["qtr4"] = $this->qtrStatsModel->insert($data["qtr"]);
			$gameStatistics[1]["ot"]  = $this->qtrStatsModel->insert($data["qtr"]);

			$this->gameStatisticsModel->update($gameStatistics[1]);

		}
		$query = $this->db->query('SELECT * FROM lssc.team;');
		$data["Teams"] = $query->result_array();		
	
		$_SESSION["Teams"] = $data["Teams"];
		$query = $this->db->query('SELECT MAX(overTimeQuarterNo), MAX(gameNo) FROM lssc.gamestatistics;');
		$data["QTRNO"] = $query->result_array();		
		$_SESSION["QTRNO"] = $data["QTRNO"];
		$this->load->view('admin-addmatch',$data);

	}
	
}
