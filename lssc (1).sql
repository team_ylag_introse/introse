-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 14, 2016 at 10:43 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lssc`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
`idaccounts` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`idaccounts`, `username`, `password`) VALUES
(1, 'comish', '12345'),
(2, 'boy', '12345'),
(3, 'girl', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `bracket`
--

CREATE TABLE IF NOT EXISTS `bracket` (
  `teamId` int(11) NOT NULL,
  `bracket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bracket`
--

INSERT INTO `bracket` (`teamId`, `bracket`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 5),
(5, 4),
(6, 6),
(7, 2),
(8, 7),
(9, 3),
(10, 4),
(11, 5),
(12, 3),
(13, 8);

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
`idGame` int(11) NOT NULL,
  `date` date NOT NULL,
  `teamA` int(11) DEFAULT NULL,
  `teamB` int(11) DEFAULT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `court` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=545 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`idGame`, `date`, `teamA`, `teamB`, `start`, `end`, `court`) VALUES
(417, '2016-04-16', 1, 8, '10:00:00', '11:00:00', 'A'),
(418, '2016-04-16', 13, 1, '11:00:00', '12:00:00', 'A'),
(419, '2016-04-16', 3, 1, '12:00:00', '13:00:00', 'A'),
(420, '2016-04-16', 7, 1, '13:00:00', '14:00:00', 'A'),
(421, '2016-04-16', 2, 1, '14:00:00', '15:00:00', 'A'),
(422, '2016-04-16', 1, 2, '15:00:00', '16:00:00', 'A'),
(423, '2016-04-16', 0, 0, '16:00:00', '17:00:00', 'A'),
(424, '2016-04-16', 0, 0, '17:00:00', '18:00:00', 'A'),
(425, '2016-04-16', 3, 4, '10:00:00', '11:00:00', 'B'),
(426, '2016-04-16', 0, 0, '11:00:00', '12:00:00', 'B'),
(427, '2016-04-16', 0, 0, '12:00:00', '13:00:00', 'B'),
(428, '2016-04-16', 0, 0, '13:00:00', '14:00:00', 'B'),
(429, '2016-04-16', 0, 0, '14:00:00', '15:00:00', 'B'),
(430, '2016-04-16', 0, 0, '15:00:00', '16:00:00', 'B'),
(431, '2016-04-16', 0, 0, '16:00:00', '17:00:00', 'B'),
(432, '2016-04-16', 0, 0, '17:00:00', '18:00:00', 'B'),
(433, '2016-04-22', 1, 2, '10:00:00', '11:00:00', 'A'),
(434, '2016-04-22', 0, 0, '11:00:00', '12:00:00', 'A'),
(435, '2016-04-22', 0, 0, '12:00:00', '13:00:00', 'A'),
(436, '2016-04-22', 0, 0, '13:00:00', '14:00:00', 'A'),
(437, '2016-04-22', 0, 0, '14:00:00', '15:00:00', 'A'),
(438, '2016-04-22', 0, 0, '15:00:00', '16:00:00', 'A'),
(439, '2016-04-22', 0, 0, '16:00:00', '17:00:00', 'A'),
(440, '2016-04-22', 0, 0, '17:00:00', '18:00:00', 'A'),
(441, '2016-04-22', 0, 0, '10:00:00', '11:00:00', 'B'),
(442, '2016-04-22', 0, 0, '11:00:00', '12:00:00', 'B'),
(443, '2016-04-22', 0, 0, '12:00:00', '13:00:00', 'B'),
(444, '2016-04-22', 0, 0, '13:00:00', '14:00:00', 'B'),
(445, '2016-04-22', 0, 0, '14:00:00', '15:00:00', 'B'),
(446, '2016-04-22', 0, 0, '15:00:00', '16:00:00', 'B'),
(447, '2016-04-22', 0, 0, '16:00:00', '17:00:00', 'B'),
(448, '2016-04-22', 0, 0, '17:00:00', '18:00:00', 'B'),
(465, '2016-04-23', 0, 0, '10:00:00', '11:00:00', 'A'),
(466, '2016-04-23', 0, 0, '11:00:00', '12:00:00', 'A'),
(467, '2016-04-23', 0, 0, '12:00:00', '13:00:00', 'A'),
(468, '2016-04-23', 0, 0, '13:00:00', '14:00:00', 'A'),
(469, '2016-04-23', 0, 0, '14:00:00', '15:00:00', 'A'),
(470, '2016-04-23', 0, 0, '15:00:00', '16:00:00', 'A'),
(471, '2016-04-23', 0, 0, '16:00:00', '17:00:00', 'A'),
(472, '2016-04-23', 0, 0, '17:00:00', '18:00:00', 'A'),
(473, '2016-04-23', 0, 0, '10:00:00', '11:00:00', 'B'),
(474, '2016-04-23', 0, 0, '11:00:00', '12:00:00', 'B'),
(475, '2016-04-23', 0, 0, '12:00:00', '13:00:00', 'B'),
(476, '2016-04-23', 0, 0, '13:00:00', '14:00:00', 'B'),
(477, '2016-04-23', 0, 0, '14:00:00', '15:00:00', 'B'),
(478, '2016-04-23', 0, 0, '15:00:00', '16:00:00', 'B'),
(479, '2016-04-23', 0, 0, '16:00:00', '17:00:00', 'B'),
(480, '2016-04-23', 0, 0, '17:00:00', '18:00:00', 'B'),
(481, '2016-05-28', 0, 0, '10:00:00', '11:00:00', 'A'),
(482, '2016-05-28', 0, 0, '11:00:00', '12:00:00', 'A'),
(483, '2016-05-28', 0, 0, '12:00:00', '13:00:00', 'A'),
(484, '2016-05-28', 0, 0, '13:00:00', '14:00:00', 'A'),
(485, '2016-05-28', 0, 0, '14:00:00', '15:00:00', 'A'),
(486, '2016-05-28', 0, 0, '15:00:00', '16:00:00', 'A'),
(487, '2016-05-28', 0, 0, '16:00:00', '17:00:00', 'A'),
(488, '2016-05-28', 0, 0, '17:00:00', '18:00:00', 'A'),
(489, '2016-05-28', 0, 0, '10:00:00', '11:00:00', 'B'),
(490, '2016-05-28', 0, 0, '11:00:00', '12:00:00', 'B'),
(491, '2016-05-28', 0, 0, '12:00:00', '13:00:00', 'B'),
(492, '2016-05-28', 0, 0, '13:00:00', '14:00:00', 'B'),
(493, '2016-05-28', 0, 0, '14:00:00', '15:00:00', 'B'),
(494, '2016-05-28', 0, 0, '15:00:00', '16:00:00', 'B'),
(495, '2016-05-28', 0, 0, '16:00:00', '17:00:00', 'B'),
(496, '2016-05-28', 0, 0, '17:00:00', '18:00:00', 'B'),
(513, '2016-04-15', 2, 1, '10:00:00', '11:00:00', 'A'),
(514, '2016-04-15', 0, 0, '11:00:00', '12:00:00', 'A'),
(515, '2016-04-15', 0, 0, '12:00:00', '13:00:00', 'A'),
(516, '2016-04-15', 0, 0, '13:00:00', '14:00:00', 'A'),
(517, '2016-04-15', 0, 0, '14:00:00', '15:00:00', 'A'),
(518, '2016-04-15', 0, 0, '15:00:00', '16:00:00', 'A'),
(519, '2016-04-15', 0, 0, '16:00:00', '17:00:00', 'A'),
(520, '2016-04-15', 0, 0, '17:00:00', '18:00:00', 'A'),
(521, '2016-04-15', 0, 0, '10:00:00', '11:00:00', 'B'),
(522, '2016-04-15', 0, 0, '11:00:00', '12:00:00', 'B'),
(523, '2016-04-15', 0, 0, '12:00:00', '13:00:00', 'B'),
(524, '2016-04-15', 0, 0, '13:00:00', '14:00:00', 'B'),
(525, '2016-04-15', 0, 0, '14:00:00', '15:00:00', 'B'),
(526, '2016-04-15', 0, 0, '15:00:00', '16:00:00', 'B'),
(527, '2016-04-15', 0, 0, '16:00:00', '17:00:00', 'B'),
(528, '2016-04-15', 0, 0, '17:00:00', '18:00:00', 'B'),
(529, '2016-04-01', 1, 2, '10:00:00', '11:00:00', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `gamestatistics`
--

CREATE TABLE IF NOT EXISTS `gamestatistics` (
`idgameStatistics` int(11) NOT NULL,
  `idTeam` int(11) NOT NULL,
  `firstQuarterNo` int(11) DEFAULT NULL,
  `secondQuarterNo` int(11) DEFAULT NULL,
  `thirdQuarterNo` int(11) DEFAULT NULL,
  `fourthQuarterNo` int(11) DEFAULT NULL,
  `overTimeQuarterNo` int(11) DEFAULT NULL,
  `gameNo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gamestatistics`
--

INSERT INTO `gamestatistics` (`idgameStatistics`, `idTeam`, `firstQuarterNo`, `secondQuarterNo`, `thirdQuarterNo`, `fourthQuarterNo`, `overTimeQuarterNo`, `gameNo`) VALUES
(1, 2, 1, 2, 3, 4, 5, 417),
(2, 1, 6, 7, 8, 9, 10, 417),
(3, 3, 11, 12, 13, 14, 15, 425),
(4, 4, 16, 17, 18, 19, 20, 425),
(5, 2, 21, 22, 23, 24, 25, 421),
(6, 1, 26, 27, 28, 29, 30, 421),
(7, 3, 31, 32, 33, 34, 35, 419),
(8, 1, 36, 37, 38, 39, 40, 419),
(9, 13, 41, 42, 43, 44, 45, 418),
(10, 1, 46, 47, 48, 49, 50, 418),
(11, 3, 51, 52, 53, 54, 55, 449),
(12, 6, 56, 57, 58, 59, 60, 449),
(13, 1, 61, 62, 63, 64, 65, 529),
(14, 2, 66, 67, 68, 69, 70, 529),
(15, 2, 71, 72, 73, 74, 75, 513),
(16, 1, 76, 77, 78, 79, 80, 513);

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
`idPlayer` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `middleName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `idNo` varchar(45) NOT NULL,
  `uniformSize` varchar(45) NOT NULL,
  `contactNumber` varchar(45) NOT NULL,
  `uniformNumber` varchar(45) NOT NULL,
  `teamNo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`idPlayer`, `firstName`, `middleName`, `lastName`, `idNo`, `uniformSize`, `contactNumber`, `uniformNumber`, `teamNo`) VALUES
(1, 'Herbert', 'F', 'Leong', '11300000', 'L', '09220000000', '07', 1),
(2, 'Rod', 'F', 'Arceo', '11354123', 'M', '09175438811', '19', 1),
(3, 'Paolo', 'H', 'Ylag', '11341123', 'M', '09153215412', '10', 1),
(4, 'Ace', 'T', 'Carlos', '11312431', 'L', '09198129911', '55', 1),
(5, 'Gaby', 'T', 'Yusay', '113123152', 'M', '09175411133', '69', 1),
(6, 'Carlo', 'G', 'Izumiya', '11316942', 'S', '09177531199', '09', 2),
(7, 'Kevin', 'D', 'Durant', '11232143', 'M', '09152228976', '35', 2),
(8, 'Lebron', 'J', 'James', '11223232', 'L', '09172113322', '23', 2),
(9, 'Stephen', 'C', 'Curry', '11330303', 'S', '09174321122', '30', 2),
(10, 'Chris', 'P', 'Paul', '11331113', 'S', '09175421133', '3', 2),
(11, 'Q', 'Q', 'Q', '1', 'S', '1', '1', 13),
(12, 'W', 'W', 'W', '4', 'XS', '9', '23', 13),
(13, 'TT', 'T', 'RT', '4', 'S', '8', '6', 13),
(14, 'T', 'T', 'T', '444', 'M', '5', '97', 13),
(15, 'Y', 'Y', 'Y', '8', 'XL', '67', '99', 13),
(16, 'Y', 'Y', 'Y', '8', 'M', '4', '76', 13),
(17, 'I', 'I', 'I', '9', 'XS', '9', '93', 13),
(18, 'K', 'K', 'K', '9', 'S', '9', '23', 13),
(19, 'C', 'C', 'C', '5', 'S', '6', '71', 13),
(20, 'E', 'E', 'E', '2', 'XS', '3', '5', 13),
(21, 'G', 'H', 'R', '6', 'S', '7', '9', 13),
(22, 'W', 'F', 'D', '3', 'S', '4', '7', 13);

-- --------------------------------------------------------

--
-- Table structure for table `playerstatistics`
--

CREATE TABLE IF NOT EXISTS `playerstatistics` (
`idPlayerStatistics` int(11) NOT NULL,
  `playerID` int(11) NOT NULL,
  `totalPoints` int(11) NOT NULL DEFAULT '0',
  `freeThrows` int(11) NOT NULL DEFAULT '0',
  `rebounds` int(11) NOT NULL DEFAULT '0',
  `assist` int(11) NOT NULL DEFAULT '0',
  `blocks` int(11) NOT NULL DEFAULT '0',
  `fouls` int(11) NOT NULL DEFAULT '0',
  `threePoints` int(11) NOT NULL DEFAULT '0',
  `twoPoints` int(11) NOT NULL DEFAULT '0',
  `gameNo` int(11) NOT NULL,
  `steals` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `playerstatistics`
--

INSERT INTO `playerstatistics` (`idPlayerStatistics`, `playerID`, `totalPoints`, `freeThrows`, `rebounds`, `assist`, `blocks`, `fouls`, `threePoints`, `twoPoints`, `gameNo`, `steals`) VALUES
(1, 6, 7, 2, 1, 1, 0, 0, 1, 1, 417, 0),
(2, 7, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(3, 8, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(4, 9, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(5, 10, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(6, 1, 6, 1, 1, 1, 0, 0, 1, 1, 417, 0),
(7, 2, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(8, 3, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(9, 4, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(10, 5, 6, 1, 1, 0, 0, 0, 1, 1, 417, 0),
(11, 6, 6, 1, 0, 0, 0, 0, 1, 1, 421, 0),
(12, 7, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(13, 8, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(14, 9, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(15, 10, 3, 1, 0, 0, 0, 0, 0, 1, 421, 0),
(16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(17, 2, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(18, 3, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(19, 4, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(20, 5, 0, 0, 0, 0, 0, 0, 0, 0, 421, 0),
(21, 1, 0, 0, 0, 0, 0, 0, 0, 0, 419, 0),
(22, 2, 0, 0, 0, 0, 0, 0, 0, 0, 419, 0),
(23, 3, 0, 0, 0, 0, 0, 0, 0, 0, 419, 0),
(24, 4, 0, 0, 0, 0, 0, 0, 0, 0, 419, 0),
(25, 5, 0, 0, 0, 0, 0, 0, 0, 0, 419, 0),
(26, 11, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(27, 12, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(28, 13, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(29, 14, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(30, 15, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(31, 16, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(32, 17, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(33, 18, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(34, 19, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(35, 20, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(36, 21, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(37, 22, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(38, 1, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(39, 2, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(40, 3, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(41, 4, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(42, 5, 0, 0, 0, 0, 0, 0, 0, 0, 418, 0),
(43, 1, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(44, 2, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(45, 3, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(46, 4, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(47, 5, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(48, 6, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(49, 7, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(50, 8, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(51, 9, 6, 1, 0, 0, 0, 0, 1, 1, 529, 0),
(52, 10, 7, 2, 0, 0, 0, 0, 1, 1, 529, 0),
(53, 6, 1, 1, 0, 0, 0, 0, 0, 0, 513, 0),
(54, 7, 1, 1, 0, 0, 0, 0, 0, 0, 513, 0),
(55, 8, 1, 1, 0, 0, 0, 0, 0, 0, 513, 0),
(56, 9, 1, 1, 0, 0, 0, 0, 0, 0, 513, 0),
(57, 10, 1, 1, 0, 0, 0, 0, 0, 0, 513, 0),
(58, 1, 6, 1, 0, 0, 0, 0, 1, 1, 513, 0),
(59, 2, 6, 1, 0, 0, 0, 0, 1, 1, 513, 0),
(60, 3, 6, 1, 0, 0, 0, 0, 1, 1, 513, 0),
(61, 4, 6, 1, 0, 0, 0, 0, 1, 1, 513, 0),
(62, 5, 7, 2, 0, 0, 0, 0, 1, 1, 513, 0);

-- --------------------------------------------------------

--
-- Table structure for table `quarterstatistics`
--

CREATE TABLE IF NOT EXISTS `quarterstatistics` (
`idquarterStatistics` int(11) NOT NULL,
  `gameStatisticsNo` int(11) NOT NULL,
  `teamScore` int(11) NOT NULL DEFAULT '0',
  `teamFoul` int(11) NOT NULL DEFAULT '0',
  `teamNo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quarterstatistics`
--

INSERT INTO `quarterstatistics` (`idquarterStatistics`, `gameStatisticsNo`, `teamScore`, `teamFoul`, `teamNo`) VALUES
(1, 1, 6, 0, 2),
(2, 1, 6, 0, 2),
(3, 1, 6, 0, 2),
(4, 1, 6, 0, 2),
(5, 1, 7, 0, 2),
(6, 2, 6, 0, 1),
(7, 2, 6, 0, 1),
(8, 2, 6, 0, 1),
(9, 2, 6, 0, 1),
(10, 2, 6, 0, 1),
(11, 3, 0, 0, 3),
(12, 3, 0, 0, 3),
(13, 3, 0, 0, 3),
(14, 3, 0, 0, 3),
(15, 3, 0, 0, 3),
(16, 4, 0, 0, 4),
(17, 4, 0, 0, 4),
(18, 4, 0, 0, 4),
(19, 4, 0, 0, 4),
(20, 4, 0, 0, 4),
(21, 5, 9, 0, 2),
(22, 5, 0, 0, 2),
(23, 5, 0, 0, 2),
(24, 5, 0, 0, 2),
(25, 5, 0, 0, 2),
(26, 6, 0, 0, 1),
(27, 6, 0, 0, 1),
(28, 6, 0, 0, 1),
(29, 6, 0, 0, 1),
(30, 6, 0, 0, 1),
(31, 7, 0, 0, 3),
(32, 7, 0, 0, 3),
(33, 7, 0, 0, 3),
(34, 7, 0, 0, 3),
(35, 7, 0, 0, 3),
(36, 8, 0, 0, 1),
(37, 8, 0, 0, 1),
(38, 8, 0, 0, 1),
(39, 8, 0, 0, 1),
(40, 8, 0, 0, 1),
(41, 9, 0, 0, 13),
(42, 9, 0, 0, 13),
(43, 9, 0, 0, 13),
(44, 9, 0, 0, 13),
(45, 9, 0, 0, 13),
(46, 10, 0, 0, 1),
(47, 10, 0, 0, 1),
(48, 10, 0, 0, 1),
(49, 10, 0, 0, 1),
(50, 10, 0, 0, 1),
(51, 11, 0, 0, 3),
(52, 11, 0, 0, 3),
(53, 11, 0, 0, 3),
(54, 11, 0, 0, 3),
(55, 11, 0, 0, 3),
(56, 12, 0, 0, 6),
(57, 12, 0, 0, 6),
(58, 12, 0, 0, 6),
(59, 12, 0, 0, 6),
(60, 12, 0, 0, 6),
(61, 13, 6, 0, 1),
(62, 13, 6, 0, 1),
(63, 13, 6, 0, 1),
(64, 13, 6, 0, 1),
(65, 13, 6, 0, 1),
(66, 14, 6, 0, 2),
(67, 14, 6, 0, 2),
(68, 14, 6, 0, 2),
(69, 14, 6, 0, 2),
(70, 14, 7, 0, 2),
(71, 15, 1, 0, 2),
(72, 15, 1, 0, 2),
(73, 15, 1, 0, 2),
(74, 15, 1, 0, 2),
(75, 15, 1, 0, 2),
(76, 16, 6, 0, 1),
(77, 16, 6, 0, 1),
(78, 16, 6, 0, 1),
(79, 16, 6, 0, 1),
(80, 16, 7, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
`idTeam` int(11) NOT NULL,
  `teamName` varchar(45) NOT NULL,
  `teamCaptain` int(11) NOT NULL,
  `win` int(11) NOT NULL DEFAULT '0',
  `lose` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`idTeam`, `teamName`, `teamCaptain`, `win`, `lose`) VALUES
(1, 'CCS', 1, 12, 1),
(2, 'COB', 6, 11, 7),
(3, 'SOE', 47, 8, 4),
(4, 'CED', 0, 5, 7),
(5, 'GCOE', 0, 9, 3),
(6, 'CLA', 0, 4, 8),
(7, 'COS', 0, 2, 10),
(8, 'CSB', 0, 0, 12),
(9, 'JRU', 0, 6, 6),
(10, 'EAC', 0, 1, 11),
(11, 'STI', 0, 0, 0),
(12, 'WRX', 0, 0, 0),
(13, 'Team HERBERT', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`idaccounts`);

--
-- Indexes for table `bracket`
--
ALTER TABLE `bracket`
 ADD PRIMARY KEY (`teamId`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
 ADD PRIMARY KEY (`idGame`), ADD UNIQUE KEY `idGame_UNIQUE` (`idGame`);

--
-- Indexes for table `gamestatistics`
--
ALTER TABLE `gamestatistics`
 ADD PRIMARY KEY (`idgameStatistics`), ADD UNIQUE KEY `idgameStatistics` (`idgameStatistics`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
 ADD PRIMARY KEY (`idPlayer`), ADD UNIQUE KEY `idPlayer` (`idPlayer`);

--
-- Indexes for table `playerstatistics`
--
ALTER TABLE `playerstatistics`
 ADD PRIMARY KEY (`idPlayerStatistics`), ADD UNIQUE KEY `idPlayerStatistics` (`idPlayerStatistics`);

--
-- Indexes for table `quarterstatistics`
--
ALTER TABLE `quarterstatistics`
 ADD PRIMARY KEY (`idquarterStatistics`), ADD UNIQUE KEY `idquarterStatistics` (`idquarterStatistics`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
 ADD PRIMARY KEY (`idTeam`), ADD UNIQUE KEY `idTeam` (`idTeam`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `idaccounts` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
MODIFY `idGame` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=545;
--
-- AUTO_INCREMENT for table `gamestatistics`
--
ALTER TABLE `gamestatistics`
MODIFY `idgameStatistics` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
MODIFY `idPlayer` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `playerstatistics`
--
ALTER TABLE `playerstatistics`
MODIFY `idPlayerStatistics` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `quarterstatistics`
--
ALTER TABLE `quarterstatistics`
MODIFY `idquarterStatistics` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
MODIFY `idTeam` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
