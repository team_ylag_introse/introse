$(document).ready(function()
{
	// for the public view schedule public page
	$('#date').datepicker({
		beforeShowDay: function(date)
		{
			return [date.getDay() == 5 || date.getDay() == 6]
		},

	});

	// For picking game matches
	$('#gameDate').datepicker({
		beforeShowDay: function(date)
		{
			return [date.getDay() == 5 || date.getDay() == 6]	
		},onSelect: function(date) {
            self.location='addMatch?date='+date
        },
		minDate: new Date(),
		dateFormat: 'yy-mm-dd'
	});
	$('#gameDate2').datepicker({
		beforeShowDay: function(date)
		{
			return [date.getDay() == 5 || date.getDay() == 6]	
		},onSelect: function(date) {
            self.location='viewmatch?date='+date
        },
		dateFormat: 'yy-mm-dd'
	});

});


