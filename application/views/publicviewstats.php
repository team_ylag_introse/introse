<!DOCTYPE html>
<html>
	<head>
		<title>LSAL - Homepage</title>

		
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- View Sched CSS -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/viewsched.css');?>">

		<!-- JQUERY UI -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jquery-ui.css');?>">		

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js')?>"></script>

		<!-- JQUERY UI -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery-ui.js')?>"></script>

		<!-- Date Exclude -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/dateexclude.js')?>"></script>
		
		<meta charset="ISO-8859-1">
		
	</head>
	
	<body>
		

		<!-- NAVBAR ON THE LEFT SIDE -->
		<!-- Navbar for Public pages -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				      <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		      		
		    	</div>
		  	</div>
		</nav>
	<!-- end of .nav -->
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<div class="row">
	    		<center>
	    			<h1> TEAM <?php echo $TeamOne["team"]['teamName'];?>  VS 
	    				 TEAM <?php  echo $TeamTwo["team"]['teamName'];?></h1>
	    			<h5> Elimination Round</h5>
	    			<h5>  <?php echo "Date: ".$gameDetail[0]['date']; ?></h5>
	    			<h5>  <?php echo "Time: ".$gameDetail[0]['start']. "-" .$gameDetail[0]['end']; ?></h5>
	    			<h5>  <?php echo "Court ".$gameDetail[0]['court']; ?></h5>
	    		
	    			<h1>Team Scores</h1>
	    			<br/>
	    			
	    		</center>
	    	
	    		<!-- Players Table --> 
					<div class="row">
						<table class="table table-bordered table-condensed table-striped table-hover table-register table-score" >
      						<thead>
      							<tr>
      								<th>Team</th>
      								<th>1</th>
      								<th>2</th>
      								<th>3</th>
      								<th>4</th>
      								<th>OT</th>
      								<th>TOTAL</th>
      							</tr>

      						</thead>

      						<tbody >
								<tr class="score">
								
									<td>
										TEAM <?php 

									    			/*	echo "<pre>";
									    				print_r($TeamOne[0]["teamname"]); */

										
								        echo htmlentities($TeamOne["team"]['teamName']); 
								       


								 ?>  
																	</td>
										 
									<td>
											<?php echo htmlentities($TeamOne["quarters"]["firstQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamOne["quarters"]["secondQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamOne["quarters"]["thirdQuarter"]["teamScore"]); ?>
									</td>
									<td>
									
										<?php echo htmlentities($TeamOne["quarters"]["fourthQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php echo htmlentities($TeamOne["quarters"]["overTimeQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php $Gametotal = $TeamOne["quarters"]["firstQuarter"]["teamScore"] + $TeamOne["quarters"]["secondQuarter"]["teamScore"] + $TeamOne["quarters"]["thirdQuarter"]["teamScore"] + $TeamOne["quarters"]["fourthQuarter"]["teamScore"] + $TeamOne["quarters"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?>
									</td>
						         
						         
						         <tr class="score">
						         	<td>
										TEAM <?php  echo htmlentities($TeamTwo["team"]['teamName']);  ?>
									</td>
									
										<td>
											<?php echo htmlentities($TeamTwo["quarters"]["firstQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamTwo["quarters"]["secondQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamTwo["quarters"]["thirdQuarter"]["teamScore"]); ?>
									</td>
									<td>
									
										<?php echo htmlentities($TeamTwo["quarters"]["fourthQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php echo htmlentities($TeamTwo["quarters"]["overTimeQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php $Gametotal = $TeamTwo["quarters"]["firstQuarter"]["teamScore"] + $TeamTwo["quarters"]["secondQuarter"]["teamScore"] + $TeamTwo["quarters"]["thirdQuarter"]["teamScore"] + $TeamTwo["quarters"]["fourthQuarter"]["teamScore"] + $TeamTwo["quarters"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?>
									</td>
						                 
      						</tbody>
    					</table>
    					<br>
    					<center>
    						<h2> Player Statistics </h2>
    					</center>
    			
    					<ul class="nav nav-tabs">
						  <li class="active"><a href="#A" data-toggle="tab" aria-expanded="true"><?php  echo htmlentities($TeamOne["team"]['teamName']);  ?>  </a></li>
						  <li class=""><a href="#B" data-toggle="tab" aria-expanded="true"><?php  echo htmlentities($TeamTwo["team"]['teamName']);  ?></a></li>
						</ul>
						
						<div id="myTabContent" class="tab-content">
						  <div class="tab-pane fade active in" id="A">
						    <table class="table table-bordered table-condensed table-striped table-hover table-register table-score">
    								<thead>
    									<tr>
		      								<th>Player</th>
		      								<th>FG 2 pts</th>
		      								<th>FG 3 pts</th>
		      								<th>Free Throw</th>
		      								<th>Rebound</th>
		      								<th>Assists</th>
		      								<th>Steals</th>
		      								<th>Blocks</th>
		      								<th>Fouls</th>
		      								<th>FG Total</th>
		      							</tr>
		    						</thead>
    						<?php 
    									$twoTotal = 0;
    									$threeTotal =0;
    									$FT =0;
    									$TOT =0;
    									$ASTOT=0;
    									$STLTOT=0;
    									$BLOCKTOT=0;
    									$FOULTOT=0;
    									$PointTotal=0;
	    								$i=1;
	    							foreach($TeamOne["players"] as $player) {


	    							?>
	    							<tbody>

	    								
	    								<td>
											<?php echo ($i ).". ".htmlentities($player["firstName"])." ".htmlentities($player["lastName"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["twoPoints"]); ?>
										</td>

										<td>
											<?php echo htmlentities($player["playerStats"]["threePoints"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["freeThrows"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["rebounds"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["assist"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["steals"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["blocks"]); ?>
										</td>
										<td>
											 <?php echo htmlentities($player["playerStats"]["fouls"]); ?>
										</td>
										<td>
											<?php $total = (2* $player["playerStats"]["twoPoints"]) + (3 * $player["playerStats"]["threePoints"] )
											+ $player["playerStats"]["freeThrows"];
											echo $total ; 

											$twoTotal += 2* $player["playerStats"]["twoPoints"];
											$threeTotal += 3* $player["playerStats"]["threePoints"];
											$FT += $player["playerStats"]["freeThrows"];
											$TOT += $player["playerStats"]["rebounds"];
											$ASTOT += $player["playerStats"]["assist"];
											$STLTOT += $player["playerStats"]["steals"];
											$BLOCKTOT += $player["playerStats"]["blocks"];
											$FOULTOT += $player["playerStats"]["fouls"];
											$PointTotal += $total;
												 ?>
										</td>

										<?php 
										$i++; } 
										?> 
										<tbody>
											<td>TOTALS:</td>
											<td><?php echo $twoTotal; ?></td>
											<td><?php echo $threeTotal; ?></td>
											<td><?php echo $FT; ?></td>
											<td><?php echo $TOT; ?></td>
											<td><?php echo $ASTOT; ?></td>
											<td><?php echo $STLTOT; ?></td>
											<td><?php echo $BLOCKTOT; ?></td>
											<td><?php echo $FOULTOT; ?></td>
											<td><?php echo $PointTotal; ?></td>
								
										</tbody>
									
	    							</tbody>
    					
    							</table>

						  </div>
						  <div class="tab-pane fade" id="B">
						    <table class="table table-bordered table-condensed table-striped table-hover table-register table-score">
    								<thead>
    									<tr>
		      								<th>Player</th>
		      								<th>FG 2 pts</th>
		      								<th>FG 3 pts</th>
		      								<th>Free Throw</th>
		      								<th>Rebound</th>
		      								<th>Assists</th>
		      								<th>Steals</th>
		      								<th>Blocks</th>
		      								<th>Fouls</th>
		      								<th>FG Total</th>
		      							</tr>
		    						</thead>
    							<?php 
    									$twoTotal = 0;
    									$threeTotal =0;
    									$FT =0;
    									$TOT =0;
    									$ASTOT=0;
    									$STLTOT=0;
    									$BLOCKTOT=0;
    									$FOULTOT=0;
    									$PointTotal=0;
	    								$i=1;
	    							foreach($TeamTwo["players"] as $player) {


	    							?>
	    							<tbody>

	    								
	    								<td>
											<?php echo ($i ).". ".htmlentities($player["firstName"])." ".htmlentities($player["lastName"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["twoPoints"]); ?>
										</td>

										<td>
											<?php echo htmlentities($player["playerStats"]["threePoints"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["freeThrows"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["rebounds"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["assist"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["steals"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["blocks"]); ?>
										</td>
										<td>
											 <?php echo htmlentities($player["playerStats"]["fouls"]); ?>
										</td>
										<td>
											<?php $total =(2 * $player["playerStats"]["twoPoints"] )+ (3 * $player["playerStats"]["threePoints"] )+ $player["playerStats"]["freeThrows"];
											echo $total ; 

											$twoTotal += 2* $player["playerStats"]["twoPoints"];
											$threeTotal += 3* $player["playerStats"]["threePoints"];
											$FT += $player["playerStats"]["freeThrows"];
											$TOT += $player["playerStats"]["rebounds"];
											$ASTOT += $player["playerStats"]["assist"];
											$STLTOT += $player["playerStats"]["steals"];
											$BLOCKTOT += $player["playerStats"]["blocks"];
											$FOULTOT += $player["playerStats"]["fouls"];
											$PointTotal += $total;
												 ?>
										</td>

										<?php 
										$i++; } 
										?> 
										
										<tbody>
											<td>TOTALS:</td>
											<td><?php echo $twoTotal; ?></td>
											<td><?php echo $threeTotal; ?></td>
											<td><?php echo $FT; ?></td>
											<td><?php echo $TOT; ?></td>
											<td><?php echo $ASTOT; ?></td>
											<td><?php echo $STLTOT; ?></td>
											<td><?php echo $BLOCKTOT; ?></td>
											<td><?php echo $FOULTOT; ?></td>
											<td><?php echo $PointTotal; ?></td>
								
										</tbody>
									
	    							</tbody>
    							</table>
						  </div>
						</div>
				
					<!-- End of Players table-->

					<br> <br> <br>
					<div  class="row">
						<center>
							

							

							
						</center>

					</div>
	    		
	    		</div>
	    	</div>
	    </div>
	    
	  
 
	</body>
</html>