<html>
	<head>
		<title>LSAL - Homepage</title>
			<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- View Sched CSS -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/viewsched.css');?>">

		<!-- JQUERY UI -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jquery-ui.css');?>">		

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js')?>"></script>

		<!-- JQUERY UI -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery-ui.js')?>"></script>

		<!-- Date Exclude -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/dateexclude.js')?>"></script>




	</head>


	<body>


	<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				     <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		      		
		    	</div>
		  	</div>
		</nav>
    	<!-- NAVBAR ON LEFT END -->


	 <div class="container" >
	    	<div class="page-header" id="banner">
					<div class="row">
					<div class="jumbotron">
							<center>
	  						<!--img src="src/logo.png"  width="200" height="100"-->
	  						
	  						<h1 >Scores and Schedules</h1>
	  						
	    			<h3><input type="text" value="<?php echo $date; ?>" name="date" placeholder="yyyy/mm/dd" id="gameDate2"/></h3>
	  						</center>
	  				</div>
	  			</div>

	  			<div class="row">
					<div class="sched-body">

						<!-- one ROW BOX-->
					<?php 
					if (is_array($games) && $games[0]["teamA"]["teamName"] != "") {
						foreach ($games as $game) 
						{
								if($game["teamA"]["teamName"] != "")
								{
					 ?>
						<div class="row sched-row" >
								<!-- time -->

								<div class="col-md-3">
									<h5><?php echo "Date: " .$game["details"]["date"] ?></h5>
									<h5><?php echo "Time: " .$game["details"]["start"]. "-" .$game["details"]["end"]?></h5>
									<h5><?php echo "Court: " .$game["details"]["court"]?></h5>
									
								</div>

								<div class="col-md-6">
										<table class="table table-striped table-hover;">
											<thead style="background-color:#2c3e50; color:white;">
												<td></td>
												<td>1</td>
												<td>2</td>
												<td>3</td>
												<td>4</td>
												<td>OT</td>
												<td> FINAL </td>
											</thead>
											<tbody>
												<tr>
													<td><?php echo $game["teamA"]["teamName"]; ?></td>
													<td><?php echo htmlentities($game["teamA"]["firstQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["secondQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["thirdQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["fourthQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["overTimeQuarter"]["teamScore"]); ?></td>
													<td><?php $Gametotal = $game["teamA"]["firstQuarter"]["teamScore"] + $game["teamA"]["secondQuarter"]["teamScore"] + $game["teamA"]["thirdQuarter"]["teamScore"] + $game["teamA"]["fourthQuarter"]["teamScore"] + $game["teamA"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?> </td>
												</tr>

												<tr>
													<td><?php echo $game["teamB"]["teamName"]; ?></td>
													<td><?php echo htmlentities($game["teamB"]["firstQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["secondQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["thirdQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["fourthQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["overTimeQuarter"]["teamScore"]); ?></td>
													<td><?php $Gametotal = $game["teamB"]["firstQuarter"]["teamScore"] + $game["teamB"]["secondQuarter"]["teamScore"] + $game["teamB"]["thirdQuarter"]["teamScore"] + $game["teamB"]["fourthQuarter"]["teamScore"] + $game["teamB"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?> </td>
												</tr>
											</tbody>
										</table>
								</div>

								<!-- end of Time column-->

								<!-- VIEW GAME STATS -->
						
									<br>

									<!-- End a href game stats -->

								<div class="col-md-3">
									<h4><a href="<?php echo base_url("home/viewGameStats")."/?gameNo=".$game["details"]["idGame"]; ?>"> View Game Stats </a></h4>
								</div>


						</div> <!-- ENd of .sched-row -->

					<?php 
							}
						}
					}
					else
					{

						?>
						<center> <h1>NO MATCHES FOR THIS DATE</h1> </center>
						<?php 

					}
					?>

					</div>
				</div>

			</div>
	    </div>



	
	

	</body>





</html>