<html>
	<head>
		<title>LSAL - Homepage</title>
			<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jasny-bootstrap.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/adminpage.css');?>">

		<!-- FOR ICON RESIZING -->
		<style type="text/css">
			i
			{
				font-size: 100px !important;
			}

			.icon-text
			{
				margin-top: 60px;
				width: 190px;
			}

			.icon-div
			{
				padding-top: 50px;
			}

			.icon-div:hover
			{
				border: 1px solid ;
				border-radius: 20px;
				height: 50%;
				padding-top: 49px;
			}
		</style>

		

		<link rel="icon" type="image/ico" href="<?php echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/jasny-bootstrap.js');?>"></script>


	</head>


	<body>



		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
					<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
					
				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<div class="row">
	    			<center><h1>Welcome Back LSAL Commissioner!</h1></center>
	    		</div>


	    		<!--div class="row">
	    		game standings here 

	    		</div-->
	    		<br><br>

	    		<div class="row">
	    			<center><h3>What brings you here ? </h3></center>

	    		</div>

	    		<br><br><br>

	    		<div class="row">
	    			<a href="<?php echo base_url('/admin/addmatch'); ?>">
		    			<div class="col-md-3 icon-div">
		    				<center>
		    					<i class="glyphicon glyphicon-calendar"></i>
		    					<h4 class="icon-text">Create Game Schedules for the week</h4>
		    				</center>

		    			</div>
	    			</a>

	    			<a href="<?php echo base_url('/admin/viewmatch'); ?>">
		    			<div class="col-md-3 icon-div">
		    				<center>
		    					<i class="glyphicon glyphicon-pencil"></i>
		    					<h4 class="icon-text">View Matches and Edit Game Statistics</h4>
		    				</center>
		    				
		    			</div>
		    		</a>
		    		<a href="<?php echo base_url('/admin/viewBracket'); ?>">

		    			<div class="col-md-3 icon-div">
			    			<center>
			    				<i class="glyphicon glyphicon-edit"></i>
		    					<h4 class="icon-text">Team Bracketing</h4>	
			    			</center>
		    				
		    			</div>
	    			</a>
	    			<a href="<?php echo base_url('/admin/viewTeams'); ?>">

		    			<div class="col-md-3 icon-div">
			    			<center>
			    				<i class="glyphicon glyphicon-th"></i>
		    					<h4 class="icon-text">View Teams</h4>	
			    			</center>
		    				
		    			</div>
	    			</a>
		    		

	    		</div>
	    	</div> <!-- .page-header-->

	    </div> <!-- .container -->


	
	

	</body>





</html>