<html>
	<head>
		<title>LSAL - Homepage</title>
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jasny-bootstrap.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/adminpage.css');?>">

		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jquery-ui.css');?>">	


		<link rel="icon" type="image/ico" href="<?php echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/jasny-bootstrap.js');?>"></script>
		
		<script type="text/javascript" src="<?php echo base_url('resources/js/checkfield.js');?>"></script> 

		<!-- JQUERY UI -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery-ui.js')?>"></script>

		<!-- Date Exclude -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/dateexclude.js')?>"></script>
		

		
		<meta charset="ISO-8859-1">
	
	</head>


	<body>



		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
					<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
					
				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<h1> Schedule Making </h1>
	    		<?php 
					if (!empty($error))
					{
						echo 	"<div class='panel panel-danger'>
  									<div class='panel-heading'>
    									<h3 class='panel-title'><span class='glyphicon glyphicon-remove'></span>&nbsp;Error</h3>
  									</div>
  									<div class='panel-body'>";
    					echo $error;
  						echo 		"</div>
								</div>";
						
					}
					else if (!empty($updated)) {
						echo "<div class='alert alert-dismissible alert-info'>
						  		<button type='button' class='close' data-dismiss='alert'>x</button>
						  		<center><strong>Data has been saved successfully!</strong></center> 
								</div>";}
				  ?>
		    </div>

			<form method = "POST" action="<?php echo base_url("/admin/addMatch"); ?>"> 	
		    <div class="row">

	    		<div class="form-group col-md-4">
	    			<label for="date">Date </label>
	    			
	    			<h3><input type="text" value="<?php echo $date; ?>" name="date" placeholder="yyyy/mm/dd" id="gameDate"/></h3>
	    		</div>
	    	</div>

	    	<br> <br>
	    		
		    	<div class="row col-md-12">
		   			<div class="row">
		   				<center><h3> COURT A</h3></center>
		   			</div>
		   			<br>
		   			<?php 	$i = 0;
		   					foreach(range(intval('9:00:00'),intval('16:00:00')) as $time) {
		   					 ?>
		   			
		   			
		   			<div class="row">
		   				<div class="col-md-2">
		   						<input type="hidden" name="game[<?php echo $i; ?>][start]" value ="<?php echo date("H:00:00", mktime($time+1)) ?>">
		   						<input type="hidden" name="game[<?php echo $i; ?>][end]" value ="<?php echo date("H:00:00", mktime($time+2)) ?>">
		   						<input type="hidden" name="game[<?php echo $i; ?>][court]" value ="A">
		   						<h5> <?php  echo date("H:00", mktime($time+1)). '-' .date("H:00", mktime($time+2)) .'<br>'; ?> </h5>
		   				</div>


		   			
						<div class="col-md-4">

							<select id="second-choice" name="game[<?php echo $i; ?>][teamA]" class="form-control">
								<option selected >------------</option>

							<?php foreach ($teams as $team) {
								# code...
							 ?>
								<option value="<?php echo $team["idTeam"] ?>" <?php if($game[$i]["teamA"] == $team["idTeam"]) echo "selected" ?>><?php echo $team["teamName"]; ?></option>
							<?php } ?>
							</select>
						</div>

						<div class="col-md-2">
							<center><h5> VS </h5></center>
						</div>


						<div class="col-md-4">

							<select id="second-choice" name="game[<?php echo $i; ?>][teamB]" class="form-control">
								<option selected >------------</option>

							<?php foreach ($teams as $team) {
								# code...
							 ?>
								<option value="<?php echo $team["idTeam"] ?>" <?php if($game[$i]["teamB"] == $team["idTeam"]) echo "selected" ?>><?php echo $team["teamName"]; ?></option>
							<?php } ?>
							</select>
						</div>
					</div>
					<br>
					<?php $i++;}?>
				</div>
				
					
				

				<br><br><div class="row">
		   				<center><h3> COURT B</h3></center>
		   			</div>
		   			<br>
		   			<?php 
		   					foreach(range(intval('9:00:00'),intval('16:00:00')) as $time) {
		   					 ?>
		   			
		   			
		   			<div class="row">
		   				<div class="col-md-2">
		   						<input type="hidden" name="game[<?php echo $i; ?>][start]" value ="<?php echo date("H:00:00", mktime($time+1)) ?>">
		   						<input type="hidden" name="game[<?php echo $i; ?>][end]" value ="<?php echo date("H:00:00", mktime($time+2)) ?>">
		   						<input type="hidden" name="game[<?php echo $i; ?>][court]" value ="B">
		   						<h5> <?php  echo date("H:00", mktime($time+1)). '-' .date("H:00", mktime($time+2)) .'<br>'; ?> </h5>
		   				</div>


		   			
						<div class="col-md-4">

							<select id="second-choice" name="game[<?php echo $i; ?>][teamA]" class="form-control">
								<option selected >------------</option>

							<?php foreach ($teams as $team) {
								# code...
							 ?>
								<option value="<?php echo $team["idTeam"] ?>" <?php if($game[$i]["teamA"] == $team["idTeam"]) echo "selected" ?>><?php echo $team["teamName"]; ?></option>
							<?php } ?>
							</select>
						</div>

						<div class="col-md-2">
							<center><h5> VS </h5></center>
						</div>


						<div class="col-md-4">

							<select id="second-choice" name="game[<?php echo $i; ?>][teamB]" class="form-control">
								<option selected >------------</option>

							<?php foreach ($teams as $team) {
								# code...
							 ?>
								<option value="<?php echo $team["idTeam"] ?>" <?php if($game[$i]["teamB"] == $team["idTeam"]) echo "selected" ?>><?php echo $team["teamName"]; ?></option>
							<?php } ?>
							</select>
						</div>
					</div>
					<br>
					<?php $i++;}?>
				</div>
				
					
				

				<br><br>
	    			<div  class="row">
	    				<center><button name="submit" value="submit"  class="btn btn-success"type="submit">Create Schedule</button></center>
					</div>
			
					</form>
	    </div>

	</body>
</html>