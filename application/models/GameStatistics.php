<?php 
class GameStatistics extends CI_Model {


	public function __construct()
	{
		parent::__construct();

	}

	public function insert($gameStatistics)
	{
		$data = array(
			'idTeam' => $gameStatistics["idTeam"],
			'gameNo' => $gameStatistics["gameNo"]
		);
		$this->db->insert('gamestatistics', $data); 
		return $this->db->insert_id();
	}
	public function deleteWhereGame($game){

		$data = array(
			'gameNo' => $game
		);
		$this->db->delete('quarterstatistics', $data); 
	}
	public function update($gameStatistics)
	{
		$data = array(
			'firstQuarterNo' => $gameStatistics["qtr1"],
			'secondQuarterNo' => $gameStatistics["qtr2"],
			'thirdQuarterNo' => $gameStatistics["qtr3"],
			'fourthQuarterNo' => $gameStatistics["qtr4"],
			'overTimeQuarterNo' => $gameStatistics["ot"]
		);
		$this->db->where('idgameStatistics	', $gameStatistics["id"]);
		$this->db->update('gamestatistics', $data); 
	}

	public function get_where_gameNo($id, $teamNo)
	{
		$this->db->select('*');
		$this->db->where('gameNo', $id);
		$this->db->where('idTeam', $teamNo);
		$query = $this->db->get('gamestatistics');
		return $query;
	}

} 
?>