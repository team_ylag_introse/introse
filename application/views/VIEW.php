<!DOCTYPE html>
<html>
	<head>
		<title>LSAL - Homepage</title>
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jasny-bootstrap.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/adminpage.css');?>">



		

		<link rel="icon" type="image/ico" href="<?php echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/jasny-bootstrap.js');?>"></script>
	
		
		<meta charset="ISO-8859-1">
		
	</head>
	
	<body>
		

		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
					<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
				
			 
			
				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<div class="row">
	    		<center>
	    			<h1> TEAM <?php echo $TeamOne["team"]['teamName'];?>  VS 
	    				 TEAM <?php  echo $TeamTwo["team"]['teamName'];?></h1>
	    			<h5> Elimination Round</h5>
	    			<h5>  <?php echo "Date: ".$gameDetail[0]['date']; ?></h5>
	    			<h5>  <?php echo "Time: ".$gameDetail[0]['start']. "-" .$gameDetail[0]['end']; ?></h5>
	    			<h5>  <?php echo "Court ".$gameDetail[0]['court']; ?></h5>
	    		
	    			<h1>Team Scores</h1>
	    			<br/>
	    			
	    		</center>
	    	
	    		<!-- Players Table --> 
					<div class="row">
						<table class="table table-bordered table-condensed table-striped table-hover table-register table-score" >
      						<thead>
      							<tr>
      								<th>Team</th>
      								<th>1</th>
      								<th>2</th>
      								<th>3</th>
      								<th>4</th>
      								<th>OT</th>
      								<th>TOTAL</th>
      							</tr>

      						</thead>

      						<tbody >
								<tr class="score">
								
									<td>
										TEAM <?php echo htmlentities($TeamOne["team"]['teamName']); ?>  
									</td>
										 
									<td>
										<?php echo htmlentities($TeamOne["quarters"]["firstQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamOne["quarters"]["secondQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamOne["quarters"]["thirdQuarter"]["teamScore"]); ?>
									</td>
									<td>
									
										<?php echo htmlentities($TeamOne["quarters"]["fourthQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php echo htmlentities($TeamOne["quarters"]["overTimeQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php $Gametotal = $TeamOne["quarters"]["firstQuarter"]["teamScore"] + $TeamOne["quarters"]["secondQuarter"]["teamScore"] + $TeamOne["quarters"]["thirdQuarter"]["teamScore"] + $TeamOne["quarters"]["fourthQuarter"]["teamScore"] + $TeamOne["quarters"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?>
									</td>
						         
						         
						         <tr class="score">
						         	<td>
										TEAM <?php  echo htmlentities($TeamTwo["team"]['teamName']);  ?>
									</td>
									
										<td>
											<?php echo htmlentities($TeamTwo["quarters"]["firstQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamTwo["quarters"]["secondQuarter"]["teamScore"]); ?>
									</td>
									
									<td>
										 <?php echo htmlentities($TeamTwo["quarters"]["thirdQuarter"]["teamScore"]); ?>
									</td>
									<td>
									
										<?php echo htmlentities($TeamTwo["quarters"]["fourthQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php echo htmlentities($TeamTwo["quarters"]["overTimeQuarter"]["teamScore"]); ?>
									</td>
									<td>
										<?php $Gametotal = $TeamTwo["quarters"]["firstQuarter"]["teamScore"] + $TeamTwo["quarters"]["secondQuarter"]["teamScore"] + $TeamTwo["quarters"]["thirdQuarter"]["teamScore"] + $TeamTwo["quarters"]["fourthQuarter"]["teamScore"] + $TeamTwo["quarters"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?>
									</td>
						                 
      						</tbody>
    					</table>
    					<br>
    					<center>
    						<h2> Player Statistics </h2>
    					</center>
    			
    					<ul class="nav nav-tabs">
						  <li class="active"><a href="#A" data-toggle="tab" aria-expanded="true"><?php  echo htmlentities($TeamOne["team"]['teamName']);  ?>  </a></li>
						  <li class=""><a href="#B" data-toggle="tab" aria-expanded="true"><?php  echo htmlentities($TeamTwo["team"]['teamName']);  ?></a></li>
						</ul>
						
						<div id="myTabContent" class="tab-content">
						  <div class="tab-pane fade active in" id="A">
						    <table class="table table-bordered table-condensed table-striped table-hover table-register table-score">
    								<thead>
    									<tr>
		      								<th>Player</th>
		      								<th>FG 2 pts</th>
		      								<th>FG 3 pts</th>
		      								<th>Free Throw</th>
		      								<th>Rebound</th>
		      								<th>Assists</th>
		      								<th>Steals</th>
		      								<th>Blocks</th>
		      								<th>Fouls</th>
		      								<th>FG Total</th>
		      							</tr>
		    						</thead>
    						<?php 
    									$twoTotal = 0;
    									$threeTotal =0;
    									$FT =0;
    									$TOT =0;
    									$ASTOT=0;
    									$STLTOT=0;
    									$BLOCKTOT=0;
    									$FOULTOT=0;
    									$PointTotal=0;
	    								$i=1;
	    							foreach($TeamOne["players"] as $player) {


	    							?>
	    							<tbody>

	    								
	    								<td>
											<?php echo ($i ).". ".htmlentities($player["firstName"])." ".htmlentities($player["lastName"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["twoPoints"]); ?>
										</td>

										<td>
											<?php echo htmlentities($player["playerStats"]["threePoints"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["freeThrows"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["rebounds"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["assist"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["steals"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["blocks"]); ?>
										</td>
										<td>
											 <?php echo htmlentities($player["playerStats"]["fouls"]); ?>
										</td>
										<td>
											<?php $total = (2* $player["playerStats"]["twoPoints"]) + (3 * $player["playerStats"]["threePoints"] )
											+ $player["playerStats"]["freeThrows"];
											echo $total ; 

											$twoTotal += 2* $player["playerStats"]["twoPoints"];
											$threeTotal += 3* $player["playerStats"]["threePoints"];
											$FT += $player["playerStats"]["freeThrows"];
											$TOT += $player["playerStats"]["rebounds"];
											$ASTOT += $player["playerStats"]["assist"];
											$STLTOT += $player["playerStats"]["steals"];
											$BLOCKTOT += $player["playerStats"]["blocks"];
											$FOULTOT += $player["playerStats"]["fouls"];
											$PointTotal += $total;
												 ?>
										</td>

										<?php 
										$i++; } 
										?> 
										<tbody>
											<td>TOTALS:</td>
											<td><?php echo $twoTotal; ?></td>
											<td><?php echo $threeTotal; ?></td>
											<td><?php echo $FT; ?></td>
											<td><?php echo $TOT; ?></td>
											<td><?php echo $ASTOT; ?></td>
											<td><?php echo $STLTOT; ?></td>
											<td><?php echo $BLOCKTOT; ?></td>
											<td><?php echo $FOULTOT; ?></td>
											<td><?php echo $PointTotal; ?></td>
								
										</tbody>
									
	    							</tbody>
    					
    							</table>

						  </div>
						  <div class="tab-pane fade" id="B">
						    <table class="table table-bordered table-condensed table-striped table-hover table-register table-score">
    								<thead>
    									<tr>
		      								<th>Player</th>
		      								<th>FG 2 pts</th>
		      								<th>FG 3 pts</th>
		      								<th>Free Throw</th>
		      								<th>Rebound</th>
		      								<th>Assists</th>
		      								<th>Steals</th>
		      								<th>Blocks</th>
		      								<th>Fouls</th>
		      								<th>FG Total</th>
		      							</tr>
		    						</thead>
    							<?php 
    									$twoTotal = 0;
    									$threeTotal =0;
    									$FT =0;
    									$TOT =0;
    									$ASTOT=0;
    									$STLTOT=0;
    									$BLOCKTOT=0;
    									$FOULTOT=0;
    									$PointTotal=0;
	    								$i=1;
	    							foreach($TeamTwo["players"] as $player) {


	    							?>
	    							<tbody>

	    								
	    								<td>
											<?php echo ($i ).". ".htmlentities($player["firstName"])." ".htmlentities($player["lastName"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["twoPoints"]); ?>
										</td>

										<td>
											<?php echo htmlentities($player["playerStats"]["threePoints"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["freeThrows"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["rebounds"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["assist"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["steals"]); ?>
										</td>
										<td>
											<?php echo htmlentities($player["playerStats"]["blocks"]); ?>
										</td>
										<td>
											 <?php echo htmlentities($player["playerStats"]["fouls"]); ?>
										</td>
										<td>
											<?php $total =(2 * $player["playerStats"]["twoPoints"] )+ (3 * $player["playerStats"]["threePoints"] )+ $player["playerStats"]["freeThrows"];
											echo $total ; 

											$twoTotal += 2* $player["playerStats"]["twoPoints"];
											$threeTotal += 3* $player["playerStats"]["threePoints"];
											$FT += $player["playerStats"]["freeThrows"];
											$TOT += $player["playerStats"]["rebounds"];
											$ASTOT += $player["playerStats"]["assist"];
											$STLTOT += $player["playerStats"]["steals"];
											$BLOCKTOT += $player["playerStats"]["blocks"];
											$FOULTOT += $player["playerStats"]["fouls"];
											$PointTotal += $total;
												 ?>
										</td>

										<?php 
										$i++; } 
										?> 
										
										<tbody>
											<td>TOTALS:</td>
											<td><?php echo $twoTotal; ?></td>
											<td><?php echo $threeTotal; ?></td>
											<td><?php echo $FT; ?></td>
											<td><?php echo $TOT; ?></td>
											<td><?php echo $ASTOT; ?></td>
											<td><?php echo $STLTOT; ?></td>
											<td><?php echo $BLOCKTOT; ?></td>
											<td><?php echo $FOULTOT; ?></td>
											<td><?php echo $PointTotal; ?></td>
								
										</tbody>
									
	    							</tbody>
    							</table>
						  </div>
						</div>
				
					<!-- End of Players table-->

					<br> <br> <br>
					<div  class="row">
						<center>
							

							

							
						</center>

					</div>
	    		
	    		</div>
	    	</div>
	    </div>
	    
	  
 
	</body>
</html>