<html>

	<head>
		<title>LSAL - Homepage</title>
		
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js')?>"></script>


	</head>


	<body>
	<!-- Navbar for Public pages -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				        <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		
		      		
					<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		    	</div>
		  	</div>
		</nav>
	<!-- end of .nav -->

	<!-- Page contents --> 


		<div class="container-fluid">
			<!-- need po yung page-header div -->
			

			<div class="row col-md-12">
				<center><img width="80%"  class="img-responsive" src="<?php  echo base_url('resources/images/homepage.jpg');?>"/></center>
			</div>
		</div>

		<br><br>

		<div class="container">

			<div class="col-md-6">
				<center><h3><span class="glyphicon glyphicon-stats"></span>      STANDINGS</h3></center>

				<br><br>

				<div class="col-md-8">
					<table class="table table-striped" style="margin-left: 100px" >
						<col width="500px">
						<col width="100px">
						<col width="100px">

						<thead style="background-color:#2c3e50; color:white;">
							<th width="30px">Team</th>
							<th width="30px">W</th>
							<th width="20px">L</th>
							<th width="20px">PCT</th>
						</thead>

						<tbody> <?php for($i=0;$i<10;$i++){ ?>
							<tr>
								<td><?php  echo $Teams[$i]["teamName"]?></td>
								<td><?php  echo $Teams[$i]["win"]?></td>
								<td><?php  echo $Teams[$i]["lose"]?></td>
								<td><?php  echo $Teams[$i]["winpct"]*100 ."%"?></td>

							</tr>
								<?php } ?>
						

						</tbody>
						
					</table>
				</div>

			</div>

			<div class="col-md-6">
				<center><h3><span class="glyphicon glyphicon-star-empty"></span>      ANNOUNCEMENTS</h3></center>

				<br>

				<!-- TWITTER EMBEDDED WIDGET -->
				<a class="twitter-timeline" href="https://twitter.com/LSSCDLSU" data-widget-id="718381724368048130">Tweets by @LSSCDLSU</a>

				<script>!
					function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
				</script>

			</div>

		</div>





	</body>




</html>