<?php 
class Bracket extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get()
	{
		$this->db->select('*');
		//$this->db->where('bracket', $bracket);
		$query = $this->db->get('bracket');
		return $query;
	
	}
	public function getBracket($bracket)
	{
		$this->db->select('*');
		$this->db->where('bracket', $bracket);
		$this->db->where('team.idTeam = bracket.teamId');
		$this->db->from('bracket, team');
		$query = $this->db->get();
		return $query;
	
	}
	public function insert($bracket)
	{
		$data = array(
			'teamId' => $bracket["team"],
			'bracket' => $bracket["bracket"]
		);
		$this->db->insert('bracket', $data); 
		return $this->db->insert_id();
	}
	public function deleteAll()
	{
		$data = array(
			'teamId' => $bracket["team"],
			'bracket' => $bracket["bracket"]
		);
		$this->db->delete('bracket', array('1'=>'1')); 
	}
	public function countBracket()
	{
		$this->db->select('count(*)');
		$this->db->from('bracket');
		$query = $this->db->get();
		return $query;
	
	}

	public function countTeam()
	{
		$this->db->select('count(*)');
		$this->db->from('team');
		$query = $this->db->get();
		return $query;
	
	}

} 
?>