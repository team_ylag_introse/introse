
		
$(document).ready(function()
{
	//alert($('#numPlayers').val());
	/* Intialize rows for registration table */
	/*
	for ( var i=1; i <= 12 ; i++)
	{
		addRow(i);
	}*/

	/* If number of players is specified */
	$('#numPlayers').change(function ()
	{
		var $num = $('#numPlayers').val();
		var $numRows = $('.table-register > tbody > tr').length;
		//alert($num + ' ' + $numRows);

		if($numRows > $num && $num > 11)
		{
			for(var i = 1; i <= $numRows - $num; i++ )
				$('.table-register > tbody > tr:last-child').remove();
		}

		else if ($numRows < $num && $num < 16)
		{
			for(var i = 1; i <= $num - $numRows; i++)
				addRow( $numRows + i );
		}
	});	

	function addRow( i)
	{
		if (i == 1)
		{
			$('.table-register > tbody:last-child').append('<tr class=""><td><input type="text" class="form-control" placeholder="Captain" name="player['+(i-1)+'][lastName]"/></td>' + 
									'<td><input type="text" class="form-control" name="player['+(i-1)+'][firstName]"/></td>' +
									'<td><input type="text" class="form-control" name="player['+(i-1)+'][middleName]"/></td>' +
									'<td style="width: 140px"><input type="text" class="form-control" name="player['+(i-1)+'][idNo]"/></td>' +
									'<td><input type="text" class="form-control" name="player['+(i-1)+'][contactNo]"/></td>' +
									'<td style="width: 145px;"><input type="text" class="form-control" name="player['+(i-1)+'][uniformNo]"/></td>' +
									'<td>'+
										'<select class="form-control" name="player['+(i-1)+'][uniformSize]">' +
										'<option disabled selected> -- Shirt Size -- </option>' +
											'<option>XS</option>' +
											'<option>XS</option>' +
											'<option>S</option>' +
											'<option>M</option>' +
											'<option>L</option>' +
											'<option>XL</option>' +
											'<option>XXL</option>' +
										'</select>' +
									'</td>' +
						        '</tr>');
		}
		else
		$('.table-register > tbody:last-child').append('<tr class=""><td><input type="text" class="form-control" placeholder="Player #[' + i + ']"name="player['+(i-1)+'][lastName]"/></td>' + 
									'<td><input type="text" class="form-control" name="player['+(i-1)+'][firstName]"/></td>' +
									'<td><input type="text" class="form-control" name="player['+(i-1)+'][middleName]"/></td>' +
									'<td style="width: 140px"><input type="text" class="form-control" name="player['+(i-1)+'][idNo]"/></td>' +
									'<td><input type="text" class="form-control" name="player['+(i-1)+'][contactNo]"/></td>' +
									'<td style="width: 145px;"><input type="text" class="form-control" name="player['+(i-1)+'][uniformNo]"/></td>' +
									'<td>'+
										'<select class="form-control" name="player['+(i-1)+'][uniformSize]">' +
											'<option disabled selected> -- Shirt Size -- </option>' +
											'<option>XS</option>' +
											'<option>S</option>' +
											'<option>M</option>' +
											'<option>L</option>' +
											'<option>XL</option>' +
											'<option>XXL</option>' +
										'</select>' +
									'</td>' +
						        '</tr>');
	}



});
			

		