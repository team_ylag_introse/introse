<html>
	<head>
		<title>LSAL - View Bracket</title>
		<!-- Bootstrap theme -->
<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- View Sched CSS -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/viewsched.css');?>">

		<!-- JQUERY UI -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jquery-ui.css');?>">		

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js')?>"></script>

		<!-- JQUERY UI -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery-ui.js')?>"></script>

		<!-- Date Exclude -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/dateexclude.js')?>"></script>
	</head>


	<body>
		<!-- NAVBAR ON THE LEFT SIDE -->
			<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				        <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				   
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		      		
		    	</div>
		  	</div>
		</nav>
    	<!-- NAVBAR ON LEFT END -->
    	<br>
    	<br>
    	<br>

	    <div class="container">
	    	<div class="page-header">
	    		<h1>Brackets</h1>
	    		<div class = "row">
				    	<table class = "table">
				   			<thead>
				   				<th>Total Registered Teams: <?php echo $bracket["TotalTeams"][0]["count(*)"] ?> </th>
				   				<th>Total Teams in Brackets: <?php echo $bracket["BracketTeams"][0]["count(*)"] ?> </th>
				   			</thead>
				    			
				    	</table>
				</div>
	    	</div>

	    	<div class="row">
	    		<table class = "table table-striped">



	    			<thead>

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">
	    				
	    				<th bgcolor="#FF6347"><bold>Bracket 1</bold></th>
	    				<th bgcolor="#FF6347"><b>W</b></th>
	    				<th bgcolor="#FF6347"><b>L</b></th>


	    				<th bgcolor="#00BFFF"><bold>Bracket 2</bold></th>
	    				<th bgcolor="#00BFFF"><b>W</b></th>
	    				<th bgcolor="#00BFFF"><b>L</b></th>

	    				<th bgcolor="yellow"><bold>Bracket 3</bold></th>
	    				<th bgcolor="yellow"><b>W</b></th>
	    				<th bgcolor="yellow"><b>L</b></th>

	    				<th bgcolor="#90EE90"><bold>Bracket 4</bold></th>
	    				<th bgcolor="#90EE90"><b>W</b></th>
	    				<th bgcolor="#90EE90"><b>L</b></th>
	    			</thead>


	    			<tbody>

	    			<?php 
	    				for ($ctr=0; $ctr < count($bracket[0]); $ctr++) { 
	    				# code...
	    			?>
	    				<tr>
	    				<?php for ($ctr2=0; $ctr2 < 4; $ctr2++) { 
	    					# code...
	    				 ?>
	    					<td> <a href="<?php echo base_url("home/viewTeamPage")."/?idTeam=".$bracket["$ctr2"]["$ctr"]["idTeam"]; ?>"><?php echo htmlentities($bracket[$ctr2][$ctr]["teamName"]); ?> </a></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["win"]); ?></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["lose"]); ?></td>
	    					<?php } ?>
	    				</tr>
					<?php } ?>
	    				
	    			</tbody>
	    		</table>
	    	</div>

	    	<div class="row">
	    		<table class = "table table-striped">
	    			<thead>

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">

	    				<col width="200px">
	    				<col width="5px">
	    				<col width="5px">
	    				
	    				<th bgcolor="pink"><bold>Bracket 5</bold></th>
	    				<th bgcolor="pink"><b>W</b></th>
	    				<th bgcolor="pink"><b>L</b></th>
	    				
	    				<th bgcolor="orange"><bold>Bracket 6</bold></th>
	    				<th bgcolor="orange"><b>W</b></th>
	    				<th bgcolor="orange"><b>L</b></th>

	    				<th bgcolor="#F5DEB3"><bold>Bracket 7</bold></th>
	    				<th bgcolor="#F5DEB3"><b>W</b></th>
	    				<th bgcolor="#F5DEB3"><b>L</b></th>

	    				<th bgcolor="violet"><bold>Bracket 8</bold></th>
	    				<th bgcolor="violet"><b>W</b></th>
	    				<th bgcolor="violet"><b>L</b></th>
	    			</thead>

	    			<tbody>
	    					<?php 
	    				for ($ctr=0; $ctr < count($bracket[0]); $ctr++) { 
	    				# code...
	    			?>
	    				<tr>
	    				<?php for ($ctr2=4; $ctr2 < 8; $ctr2++) { 
	    					# code...
	    				 ?>
	    					<td> <a href="<?php echo base_url("home/viewTeamPage")."/?idTeam=".$bracket["$ctr2"]["$ctr"]["idTeam"]; ?>"><?php echo htmlentities($bracket[$ctr2][$ctr]["teamName"]); ?> </a></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["win"]); ?></td>
	    					<td> <?php echo htmlentities($bracket[$ctr2][$ctr]["lose"]); ?></td>
	    					<?php } ?>
	    				</tr>
					<?php } ?>
	    				
	    			</tbody>
	    		</table>
	    	</div>
	    </div>
	</body>

</html>