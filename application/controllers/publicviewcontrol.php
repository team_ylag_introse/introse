<?php
// TRASH
defined('BASEPATH') OR exit('No direct script access allowed');

class publicviewcontrol extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

public function __construct(){
	parent::__construct();
	$this->load->helper('url');
	$this->load->database();
	$this->load->model('Statis', 'StatModel');
	$this->load->model('QtrStats', 'QtrModel');
	$this->load->model('Player', 'playerModel');
	$this->load->model('PlayerStats', 'playerStatModel');
	$this->load->model('Game', 'gameModel');
	$this->load->model('Team', 'teamModel');
	$this->load->model('GameStatistics', 'gameStatisticsModel');
	$this->load->database();
}


	public function viewMatch()
	{
		if($this->input->get("date") != "")
		{
			$date = $this->input->get("date");
		}
		else if($this->input->post("date") != "")
		{
			$date = $this->input->post("date");
		}
		else
		{
			$date = date("Y-m-d", time() + 86400 * 3);
		}

		
/*
		$query = $this->db->query(
			'SELECT distinct `game`.`idGame`,
							 `T1firstQuarter`.`teamScore` AS T1firstQuarter,
							 `T1secondQuarter`.`teamScore` AS T1secondQuarter,
							 `T1thirdQuarter`.`teamScore` AS T1thirdQuarter,
							 `T1fourthQuarter`.`teamScore` AS T1fourthQuarter, 
							 `T1overTime`.`teamScore` AS T1overTime, 
							 `T2firstQuarter`.`teamScore` AS T2firstQuarter,
							 `T2secondQuarter`.`teamScore` AS T2secondQuarter,
							 `T2thirdQuarter`.`teamScore` AS T2thirdQuarter,
							 `T2fourthQuarter`.`teamScore` AS T2fourthQuarter, 
							 `T2overTime`.`teamScore` AS T2overTime, 
							 `teamA`.`teamName` AS `teamA`, 
							 `teamB`.`teamName` AS `teamB`, 
							 `game`.`date` as `Date`,
							 `game`.`start`, 
							 `game`.`end`, 
							 `court` 
			FROM `game`, 
				 `team` AS `teamA`, 
				 `team` AS `teamB`, 
				 `gamestatistics` AS gst1, 
				 `gamestatistics` AS gst2,
				 `quarterstatistics` AS `T1firstQuarter`,
				 `quarterstatistics` AS `T1secondQuarter`,
				 `quarterstatistics` AS `T1thirdQuarter`,
				 `quarterstatistics` AS `T1fourthQuarter`,
				 `quarterstatistics` AS `T1overTime`,
				 `quarterstatistics` AS `T2firstQuarter`,
				 `quarterstatistics` AS `T2secondQuarter`,
				 `quarterstatistics` AS `T2thirdQuarter`,
				 `quarterstatistics` AS `T2fourthQuarter`,
				 `quarterstatistics` AS `T2overTime`
			WHERE `teamA`.`idTeam` = `game`.`teamA` && 
				  `teamB`.`idTeam` = `game`.`teamB` && 
				  `T1firstQuarter`.`idquarterStatistics` = `gst1`.`firstQuarterNo` && 
				  `T1secondQuarter`.`idquarterStatistics` = `gst1`.`secondQuarterNo` && 
				  `T1thirdQuarter`.`idquarterStatistics` = `gst1`.`thirdQuarterNo` && 
				  `T1fourthQuarter`.`idquarterStatistics` = `gst1`.`fourthQuarterNo`&& 
				  `T1overTime`.`idquarterStatistics` = `gst1`.`overTimeQuarterNo` &&
				  `T2firstQuarter`.`idquarterStatistics` = `gst2`.`firstQuarterNo` && 
				  `T2secondQuarter`.`idquarterStatistics` = `gst2`.`secondQuarterNo` && 
				  `T2thirdQuarter`.`idquarterStatistics` = `gst2`.`thirdQuarterNo` && 
				  `T2fourthQuarter`.`idquarterStatistics` = `gst2`.`fourthQuarterNo`&& 
				  `T2overTime`.`idquarterStatistics` = `gst2`.`overTimeQuarterNo` &&
				  `game`.`date` = ?
				  ', $date);
				  
		$data["games"] = $query->result_array();
*/




		$data["date"] = $date;
		$games = $this->gameModel->getWhereDate($date)->result_array();
		$i = 0;
		foreach ($games as $game) 
		{
			$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($game["idGame"], $game["teamA"])->result_array();
			$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($game["idGame"], $game["teamB"])->result_array();
			

			$data["games"][$i]["details"] = $game;
			
			$data["games"][$i]["teamA"]["teamName"] = $this->teamModel->get($game["teamA"])->result_array()[0]["teamName"];
			$data["games"][$i]["teamA"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamA"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];


			$data["games"][$i]["teamB"]["teamName"] = $this->teamModel->get($game["teamB"])->result_array()[0]["teamName"];

			$data["games"][$i]["teamB"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["games"][$i]["teamB"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];
			$i++;	
		}
		
		$this->load->view('admin-viewsched', $data);
	
	}

}
?>
