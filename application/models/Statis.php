<?php 
class Statis extends CI_Model {

	
	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();

	}

	

	public function validate_entry($mystats){

		if($mystats["freeThrows"] == "" || $mystats["freeThrows"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";
		}
		else if($mystats["rebounds"] == "" || $mystats["rebounds"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";
		}
		else if($mystats["assist"] == "" || $mystats["assist"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";

		}else if($mystats["fouls"] == "" || $mystats["fouls"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";
		}
		else if($mystats["blocks"] == "" || $mystats["blocks"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";

		}else if($mystats["steals"] == "" || $mystats["steals"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";
		}
		else if($mystats["threePoints"] == "" || $mystats["threePoints"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";

		}else if($mystats["twoPoints"] == "" || $mystats["twoPoints"] < 0 ){
			return "Invalid Input! Please check highlighted fields.";
		}
		
		return "";
	}      

} 
?>