<html>
	<head>
		<title>LSAL - Homepage</title>
			<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('resources/css/jasny-bootstrap.css');?>">
		

		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-adminpage.css');?>">
		<link rel="icon" type="image/ico" href="<?php echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php echo base_url('resources/js/jasny-bootstrap.js');?>"></script>


	

		
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/viewsched.css');?>">

		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jquery-ui.css');?>">		

		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery-ui.js')?>"></script>

	
		<script type="text/javascript" src="<?php  echo base_url('resources/js/dateexclude.js')?>"></script>


	</head>


	<body>



		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
				<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
		
				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	   <div class="container" style="margin-left: 230px">
	    	<div class="page-header" id="banner">
					<div class="row">
					<div class="jumbotron">
							<center>
	  						<!--img src="src/logo.png"  width="200" height="100"-->
	  						
	  						<h1 >Scores and Schedules</h1>
	  						
	    			<h3><input type="text" value="<?php echo $date; ?>" name="date" placeholder="yyyy/mm/dd" id="gameDate2"/></h3>
	  						</center>
	  				</div>
	  			</div>

	  			<div class="row">
					<div class="sched-body">

						<!-- one ROW BOX-->
					<?php 
					if(is_array($games) && $games[0]["teamA"]["teamName"] != "")
					{

						foreach ($games as $game) 
						{
								if($game["teamA"]["teamName"] != "")
								{
					 ?>
						<div class="row sched-row" >
								<!-- time -->

								<div class="col-md-3">
									<h5><?php echo "Date: " .$game["details"]["date"] ?></h5>
									<h5><?php echo "Time: " .$game["details"]["start"]. "-" .$game["details"]["end"]?></h5>
									<h5><?php echo "Court: " .$game["details"]["court"]?></h5>
									
								</div>

								<div class="col-md-6">
										<table class="table table-striped table-hover;">
											<thead style="background-color:#2c3e50; color:white;">
												<td></td>
												<td>1</td>
												<td>2</td>
												<td>3</td>
												<td>4</td>
												<td>OT</td>
												<td> FINAL </td>
											</thead>
											<tbody>
												<tr>
													<td><?php echo $game["teamA"]["teamName"]; ?></td>
													<td><?php echo htmlentities($game["teamA"]["firstQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["secondQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["thirdQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["fourthQuarter"]["teamScore"]); ?></td>
													<td><?php echo htmlentities($game["teamA"]["overTimeQuarter"]["teamScore"]); ?></td>
													<td><?php $Gametotal = $game["teamA"]["firstQuarter"]["teamScore"] + $game["teamA"]["secondQuarter"]["teamScore"] + $game["teamA"]["thirdQuarter"]["teamScore"] + $game["teamA"]["fourthQuarter"]["teamScore"] + $game["teamA"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?> </td>
												</tr>

												<tr>
													<td><?php echo $game["teamB"]["teamName"]; ?></td>
													<td><?php echo htmlentities($game["teamB"]["firstQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["secondQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["thirdQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["fourthQuarter"]["teamScore"]); ?></td>
												<td><?php echo htmlentities($game["teamB"]["overTimeQuarter"]["teamScore"]); ?></td>
													<td><?php $Gametotal = $game["teamB"]["firstQuarter"]["teamScore"] + $game["teamB"]["secondQuarter"]["teamScore"] + $game["teamB"]["thirdQuarter"]["teamScore"] + $game["teamB"]["fourthQuarter"]["teamScore"] + $game["teamB"]["overTimeQuarter"]["teamScore"];

										 echo $Gametotal; ?> </td>
												</tr>
											</tbody>
										</table>
								</div>

								<!-- end of Time column-->

								<!-- VIEW GAME STATS -->
						
									<br>

									<div class="col-md-3">
									<h4><a href="<?php echo base_url("admin/statsinput")."/?gameNo=".$game["details"]["idGame"]; ?>"> Edit Game Stats </a></h4>
								</div> <!-- End a href game stats -->

								<div class="col-md-3">
									<h4><a href="<?php echo base_url("admin/statsview")."/?gameNo=".$game["details"]["idGame"]; ?>"> View Game Stats </a></h4>
								</div>


						</div> <!-- ENd of .sched-row -->

					<?php 
							}
						}
					}
					else
					{
						?>
						<center> <h1>NO MATCHES FOR THIS DATE</h1> </center>
						<?php 

					}
					?>
					</div>
				</div>
					
			</div>
	    </div>


	
	

	</body>





</html>