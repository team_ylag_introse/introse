<?php 
class PlayerStats extends CI_Model {


	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();

	}

	public function insert($playerNo, $gameNo)
	{
		$data = array(
			'playerID' => $playerNo,
			'gameNo' => $gameNo
		);
		$this->db->insert('playerstatistics', $data); 
		return $this->db->insert_id();
	}

	public function update($stats){
		$data = array(
			'totalPoints' => $stats["totalPoints"],
			'freeThrows' => $stats["freeThrows"],
			'rebounds' => $stats["rebounds"],
			'assist' => $stats["assist"],
			'blocks' => $stats["blocks"],
			'fouls' => $stats["fouls"],
			'threePoints' => $stats["threePoints"],
			'twoPoints' => $stats["twoPoints"],
			'steals' => $stats["steals"]
		);
		$this->db->where('playerID', $stats["playerID"]);
		$this->db->where('gameNo', $stats["gameNo"]);
		$this->db->update('playerstatistics', $data);

	}
	public function get($playerId, $gameId){
		$this->db->select('*');
		$this->db->where('playerID', $playerId);
		$this->db->where('gameNo', $gameId);
		$query = $this->db->get('playerstatistics');
		return $query;
	}
	public function deleteWhereGame($game){

		$data = array(
			'gameNo' => $game
		);
		$this->db->delete('playerstatistics', $data); 
	}

	public function getStats($idPlayer,$date){
		$sql = "SELECT sum(totalPoints)/count(*) as PPG,sum(rebounds)/count(*) as RPG ,sum(assist)/count(*) as APG,sum(blocks)/count(*) as BPG,sum(steals)/count(*) as SPG  FROM lssc.playerstatistics, lssc.game where playerID = ? and game.date < ? and game.idGame = playerstatistics.gameNo";
		$query=$this->db->query($sql, array($idPlayer,$date));
		
		return $query;

	}

} 
?>