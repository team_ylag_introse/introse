<html>
	<head>
		<title>LSAL - Homepage</title>
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jasny-bootstrap.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/adminpage.css');?>">

		


		

		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php  echo base_url('resources/js/jasny-bootstrap.js');?>"></script>


	</head>


	<body>



		<!-- NAVBAR ON THE LEFT SIDE -->
		<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
					<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
					
				
			
      		</ul>

			
    	</div>
    	<!-- NAVBAR ON LEFT END -->


	    <div class="container">
	    	<div class="page-header">
	    		<center><h1>Registered Teams</h1></center>
	    	</div>
	    	<div class="row">
		    	

		    	<div class="col-md-8 col-md-offset-2">
		    		<table class ="table table-striped table-hover">
		    			<thead style="background-color:#2c3e50; color:white;">
		    			<col width="200px">
	    				

	    				<th bgcolor="#00BFFF"><center><bold>TEAM NAME</bold></center></th>
	    				

		    			</thead>

		    			<tbody>	
		    				<?php foreach($Teams as $TheTeams){ ?>
		    				<tr>	
		    					<td><center> <a href="<?php echo base_url("admin/viewTeamPage")."/?idTeam=".$TheTeams["idTeam"]; ?>"> <?php echo $TheTeams["teamName"]; ?></a></center></td>
		    					
		    				</tr>
		    				<?php } ?>
		    			</tbody>
		    		</table>
		    	</div>	

		  


		   	



	    	</div>
		</div>


		
	

	</body>





</html>