<?php 
class Account extends CI_Model {


	public function __construct()
	{
		// Call the CI_Model constructor
		parent::__construct();

	}

	public function get($username,$password){

		$sql = "SELECT * FROM lssc.accounts where username COLLATE latin1_general_cs = ? and password COLLATE latin1_general_cs = ?";	
		$query=$this->db->query($sql, array($username,$password));
		
		return $query;
	}
  

} 
?>