<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');

        $this->load->database();
        $this->load->model('Player', 'playerModel');
        $this->load->model('Team', 'teamModel');
        $this->load->library('session');
	}


	public function index()
	{
		$this->register();
        //$this->load->view("admin-viewsched", $data);

	}
	public function registration()
	{
		$this->register();
	}

	public function register(){

		$data = $this->input->post();

		$load = "registration";
		$team["teamName"] = $data["teamName"];

		$data['title'] = "LSSC : REGISTRATION";

		if($this->input->post("numPlayers") != "" && $this->input->post("numPlayers") < 16 && $this->input->post("numPlayers") > 11)
		{
			if ($team["teamName"] == "") 
			{	
				$data['error'] = "You need a team name!";
			}
			else if($this->teamModel->check($team["teamName"]))
			{
				$data["error"] = "Team Name Already Exist!";
			}
			else
			{
				$size = count($this->input->post());

				$players = $this->input->post("player");
				$ctr = 0;
				foreach ($players as $player)
				{
					$data["error"] = $this->playerModel->validate_entry($player);
					if($data["error"] != "")
					{
						if($ctr == 0)
						{
							$data["error"] = $data["error"]." for Captain";
						}
						else
						{
							$data["error"] = $data["error"]." for Player #".($ctr+1);
						}
						break;		
					}
					$ctr++;
				}
				if($data["error"] == ""){
					for ($i=0; $i < $size; $i++) 
					{ 	
						for ($x=$i+1; $x < $size; $x++) 
						{ 	
							
							if($data["player"][$x]["uniformNo"] == $data["player"][$i]["uniformNo"])
							{
								$data["error"] = "Some Players Have The Same Uniform Number";
							
								break;
							}
						}
						if($data["error"] != ""){
							break;
						}
					}
				}
				if($data["error"] == "")
				{
					$i = 0;
					$teamID = 0;
					foreach ($players as $player) 
					{
						$id = $this->playerModel->insert($player);
						if($i == 0)
						{
							$team["teamCaptain"] = $id;
							$teamID = $this->teamModel->insert($team);
							$i++;
						}
						$this->playerModel->update_teamid($teamID, $id);
					}
					$load = "registration_success";
				}
			}
		}
		else
		{
			$data['numPlayers'] = 12;
		}
        $this->load->view($load, $data);

	}


}
