<!DOCTYPE html>

<html>

	<head>
		<title>LSAL - Homepage</title>
		
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/bootstrap.js')?>"></script>


	</head>


	<body>
	<!-- Navbar for Public pages -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
				       
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		
		      		
					
		    	</div>
		  	</div>
		</nav>
	<!-- end of .nav -->

	<!-- Page contents --> 


		<div class="container">
			<!-- need po yung page-header div -->
			<div class="page-header" id="banner">

				<div class="row">
					<h1>- LOGIN -</h1>

					<br><br>

					<form action="" method="post">
						<?php 
					if (!empty($error))
					{
						echo 	"<div class='panel panel-danger'>
  									<div class='panel-heading'>
    									<h3 class='panel-title'><span class='glyphicon glyphicon-remove'></span>&nbsp;Error</h3>
  									</div>
  									<div class='panel-body'>";
    					echo $error;
  						echo 		"</div>
								</div>";
						
					}?>

					<div class="row">
						<div class="form-group col-md-6">
							<label>Username</label>
							<input type="text" id="name" name="username" class="form-control" placeholder="username"/>

						</div>
					</div>

					<div class="row">
						<div class="form-group col-md-6">
							<label>Password</label>
							<input type="password" id="password" name="password" class="form-control" placeholder="*******"/>
						</div>
					</div>

					<br><br>

					<div class="form-group">
						<button type="submit" name="submit" value="Login" class="btn btn-info">LOGIN</button>

					</div>

					</form>
				</div>
			</div>
		</div>





	</body>




</html>