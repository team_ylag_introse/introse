<html>

	<head>
		<title>LSAL - Homepage</title>
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for  admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/jasny-bootstrap.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/adminpage.css');?>">

		


		

		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js');?>"></script>

		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js');?>"></script>

		<script type="text/javascript" src="<?php  echo base_url('resources/js/jasny-bootstrap.js');?>"></script>


	</head>


	<body>
	<!-- Navbar for Public pages -->
			<div class="navmenu navmenu-default navmenu-fixed-left">
      		<a class="navmenu-brand" href="#">
      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php echo base_url('resources/images/logo.png'); ?>" height="50px"/>
      		</a>
		    <ul class="nav navmenu-nav">

		    	<li class="dropdown">
				  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin account <b class="caret"></b></a>
				  
					<ul class="dropdown-menu navmenu-nav">
						<li><a href="<?php echo base_url('/Login/logout'); ?>">Logout</a></li>
				  	</ul>
				</li>
					<li class=""><a href="<?php echo base_url('/admin/home'); ?>">Home</a></li>
					
				
			
      		</ul>

			
    	</div>
	<!-- end of .nav -->

	<!-- Page contents --> 


	<div class="container">
	    	<div class="page-header">
	    		<center>
	    			<br>
	    			<h1><?php echo $Teams[0]["teamName"]; ?></h1>
	    			<br>
	    			<h4> Wins: <?php echo $Teams[0]["win"]; ?>      Loses: <?php echo $Teams[0]["lose"]; ?> </h4>

	    		</center>
	    	</div>


	    	<div class="row">
		    	<div class="col-md-10 col-md-offset-1">

		    		<h3><span class="glyphicon glyphicon-list-alt"></span>&nbsp; &nbsp;Roster</h3>
		    		<br>
		    		<table class ="table table-striped table-hover">

		    			<col width="200px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">
		    			<col width="50px">

		    			<thead style="background-color:#2c3e50; color:white; ">
		    				<th>Player Name</th>
		    				<th>Number</th>
		    				<th>PPG</th>
		    				<th>RPG</th>
		    				<th>APG</th>
		    				<th>BPG</th>
		    				<th>SPG</th>
		    			</thead>

		    			<tbody>
		    				<?php $i=0; 
		    					foreach($Players as $player){ ?>
		    				<tr>
		    					<td><?php echo $player["firstName"]. " " .$player["lastName"];?></td>
		    					<td><?php echo $player["uniformNumber"];?></td>
		    					<td><?php echo $PerGame[$i][0]["PPG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["RPG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["APG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["BPG"];?></td>
		    					<td><?php echo $PerGame[$i][0]["SPG"];?></td>
		    				</tr>
		    				<?php $i++;} ?>
		    				
		    			</tbody>
		    		</table>
		    	</div>
	    	</div>
	    	<div class="row">
	    	<div class="col-md-10 col-md-offset-1">
		    		<h3><span class="glyphicon glyphicon-calendar"></span>&nbsp; &nbsp;Schedule</h3>
		    		<br>

		    		<table class ="table table-striped table-hover">

		    			<col width="200px">
						<col width="200px">
		    			<col width="150px">


		    			<thead style="background-color:#2c3e50; color:white;">
		    				<th>vs.</th>
		    				<th>Date and Time</th>
		    				<th>Venue</th>
		    			</thead>

		    			<tbody>
		    				<?php  $i=0;?>
		    				<?php foreach ($Schedule as $schedule){ ?>
		    				<tr>
		    					<td><a href="<?php echo base_url("admin/statsView")."/?gameNo=".$schedule["idGame"]; ?>"><?php  echo $Opponent[$i][0]["teamName"]; ?></a> </td>
		    					<td><?php  echo $schedule["date"]. "<br>" .$schedule["start"]. " - " .$schedule["end"]; ?></td>
		    					<td><?php  echo $schedule["court"]; ?></td>	
		    				</tr>
		    				<?php $i++; } ?>
		    			</tbody>
		    		</table>
		    	</div>
		    	</div>
	</div>





	</body>

	<script type="text/javascript" src="js/jquery.js"></script>


	<script type="text/javascript" src="js/bootstrap.js"></script>


</html>