<?php
// trash
@session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->helper('url');
			$this->load->database();
			$this->load->model('Account', 'accountModel');
		}

		public function logout(){
			session_destroy();

					?>
						<script> self.location = "<?php echo base_url('/Login'); ?>";</script>
					<?php
					exit();

		}
		public function index()
		{
			if($this->input->post() != NULL)
			{
				$user = $this->input->post("username");
				$pass = $this->input->post("password");

			
	
				$account= $this->accountModel->get($user,$pass)->result_array()[0];
				
				if($account == NULL)
				{
					$data['error'] = "Invalid input.";
				}
				else 
				{
					$_SESSION['username'] = $user; 
					$_SESSION['password'] = $pass;
					?>
						<script> self.location = "<?php echo base_url('/admin/home'); ?>";</script>
					<?php
					exit();
				}
			}
			$this->load->view('login', $data);
		}
}
?>