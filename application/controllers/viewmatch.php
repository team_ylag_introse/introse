<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class viewmatch extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

public function __construct(){
	parent::__construct();
	$this->load->helper('url');
	$this->load->database();
}


	public function index()
	{

	$query = $this->db->query('SELECT distinct `game`.`idGame`, `teamA`.`teamName` AS `teamA`, `teamB`.`teamName` AS `teamB`, `game`.`date` as `Date`,`game`.`start`, `game`.`end` , court FROM `game`, `team` AS `teamA`, `team` AS `teamB` WHERE `teamA`.`idTeam` = `game`.`teamA` && `teamB`.`idTeam` = `game`.`teamB`');
	$data["games"] = $query->result_array();		

	$query = $this->db->query('SELECT * FROM `team`');
	




	$this->load->view('admin-viewsched', $data);
	



	}

}
?>
