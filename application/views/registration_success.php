<html>

	<head>
		<title>LSAL - Homepage</title>
		
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js')?>"></script>


	</head>


	<body>
	<!-- Navbar for Public pages -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				        <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				   
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		      		
		    	</div>
		  	</div>
		</nav>
	<!-- end of .nav -->

	<!-- Page contents --> 


		<div class="container">
			<!-- need po yung page-header div -->
			<div class="page-header" id="banner">

				<div class="row">
					<center>
						<h1> Congratulations! </h1>
						<br>
						<br>

						<h4>
							You have successfully registered your team to the LSAL league.
							<br><br>
							Don't forget to pay the <b>REGISTRATION FEE</b> to any LSAL commissioner.
							<br><br>
							Not paying the registration fee will invalidate your team's registration.
						</h4> 
						<br>
						<br>

						<h5>Total fee for 'Team Name' </h5> 
						<h1>P 15,000 <!-- NUMBER OF PLAYERS x 1,300 --></h1>
						<h5> Number of Players : <!-- number here --> x P1,300 </h5>

						<br>
						<br>
						<h4>Goodluck and see you in the LSAL League!</h4>


					</center>



				</div> <!-- End .row -->
			</div> <!-- End .page-header -->
		</div> <!-- End .container --> 





	</body>




</html>