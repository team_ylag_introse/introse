<?php
@session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Stats extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('Statis', 'StatModel');
		$this->load->model('QtrStats', 'QtrModel');
		$this->load->model('Player', 'playerModel');
		$this->load->model('PlayerStats', 'playerStatModel');
		$this->load->model('Game', 'gameModel');
		$this->load->model('Team', 'teamModel');
		$this->load->model('GameStatistics', 'gameStatisticsModel');
	}


	public function index()
	{

		$gameNo = $this->input->get("game");
		if(!is_numeric($gameNo)){
			?>
				<script> self.location = "<?php echo base_url('/viewmatch'); ?>";</script>
			<?php
			exit();
		}

		if($this->gameModel->get($gameNo)->num_rows() == 0){
			?>
			<script> self.location = "<?php echo base_url('/viewmatch'); ?>";</script>
			<?php
			exit();
		}


		$game = $this->gameModel->get($gameNo)->result_array();
		$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
		$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
		
		$data["TeamOne"]["team"] = $this->teamModel->get($game[0]["teamA"])->result_array()[0];
		$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
		
		$i = 0;

		foreach ($data["TeamOne"]["players"] as $player) 
		{
			$data["TeamOne"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
			$i++;
		}

		$data["TeamOne"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
		$data["TeamOne"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];

		$data["TeamTwo"]["team"] = $this->teamModel->get($game[0]["teamB"])->result_array()[0];
		$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();
		$i = 0;
		foreach ($data["TeamTwo"]["players"] as $player) 
		{
			$data["TeamTwo"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
			$i++;
		}
		$data["TeamTwo"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
		$data["TeamTwo"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];
		$data["gameNo"] = $gameNo;
		$data["gameDetail"] = $game; 
		$this->load->view('statInput', $data);
	}

	
	public function statsInput()
	{	

		$data = $this->input->post();

		$gameNo = $this->input->get("gameNo");
		if(!is_numeric($gameNo) || $gameNo == ""){
			?>
				<script> self.location = "<?php //echo base_url('/viewmatch'); ?>";</script>
			<?php
			exit();
		}

		if($this->gameModel->get($gameNo)->num_rows() == 0){
			?>
			<script> self.location = "<?php //echo base_url('/viewmatch'); ?>";</script>
			<?php
			exit();
		}


		

		$game = $this->gameModel->get($gameNo)->result_array();
		$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
		$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
		

		
		if($this->input->post("submit") != "")
		{ 
			for ($i=0; $i < count($data["TeamOne"]["players"]); $i++) { 
				$teamOne["playerStats"][$i] = $data["TeamOne"]["players"][$i]["playerStats"];

			}
			for ($i=0; $i < count($data["TeamTwo"]["players"]); $i++) { 
				$teamTwo["playerStats"][$i] = $data["TeamTwo"]["players"][$i]["playerStats"];

			}

			$data["TeamOne"]["team"] = $this->teamModel->get($game[0]["teamA"])->result_array()[0];
			$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
			$data["TeamTwo"]["team"] = $this->teamModel->get($game[0]["teamB"])->result_array()[0];
			$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();
			


			for ($i=0; $i < count($data["TeamOne"]["players"]); $i++) { 
				$data["TeamOne"]["players"][$i]["playerStats"] = $teamOne["playerStats"][$i];

			}
			for ($i=0; $i < count($data["TeamTwo"]["players"]); $i++) { 
				$data["TeamTwo"]["players"][$i]["playerStats"] = $teamTwo["playerStats"][$i];

			}



			$i = 0;
			foreach ($data["TeamOne"]["quarters"] as $quarter) {
				$teamOne["quarters"][$i] = $quarter["teamScore"];
				$i++;
			}

			$i = 0;
			foreach ($data["TeamTwo"]["quarters"] as $quarter) {
				$teamTwo["quarters"][$i] = $quarter["teamScore"];
				$i++;
			}

			$gameStatTeamA = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamA"])->result_array();
			$data["TeamOne"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];




			$gameStatTeamB = $this->gameStatisticsModel->get_where_gameNo($gameNo, $game[0]["teamB"])->result_array();
			$data["TeamTwo"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];

			

			$data["TeamOne"]["quarters"]["firstQuarter"]["teamScore"] = $teamOne["quarters"][0];
			$data["TeamOne"]["quarters"]["secondQuarter"]["teamScore"] = $teamOne["quarters"][1];
			$data["TeamOne"]["quarters"]["thirdQuarter"]["teamScore"] = $teamOne["quarters"][2];
			$data["TeamOne"]["quarters"]["fourthQuarter"]["teamScore"] = $teamOne["quarters"][3];
			$data["TeamOne"]["quarters"]["overTimeQuarter"]["teamScore"] = $teamOne["quarters"][4];



			$data["TeamTwo"]["quarters"]["firstQuarter"]["teamScore"] = $teamTwo["quarters"][0];
			$data["TeamTwo"]["quarters"]["secondQuarter"]["teamScore"] = $teamTwo["quarters"][1];
			$data["TeamTwo"]["quarters"]["thirdQuarter"]["teamScore"] = $teamTwo["quarters"][2];
			$data["TeamTwo"]["quarters"]["fourthQuarter"]["teamScore"] = $teamTwo["quarters"][3];
			$data["TeamTwo"]["quarters"]["overTimeQuarter"]["teamScore"] = $teamTwo["quarters"][4];

			
			$data["error"] = "";
			$QTRTOT1 = 0;
			$QTRTOT2 = 0;
			$$GTOTAL = 0;
			$$GTOTAL2 = 0;

			foreach ($data["TeamOne"]["quarters"] as $quarterStat) 
			{
				$data["error"] = $this->QtrModel->validate_entry($quarterStat["teamScore"]);
				if($data["error"] != "")
				{
					break;
				}else{
					$QTRTOT1 += $quarterStat["teamScore"];
				}
			}
			if($data["error"] == "")
			{
				foreach ($data["TeamTwo"]["quarters"] as $quarterStat) {
					$data["error"] = $this->QtrModel->validate_entry($quarterStat["teamScore"]);
					if($data["error"] != "")
					{
						break;
					}else{
						$QTRTOT2 += $quarterStat["teamScore"];
					}
				}
			}


			if($data["error"] == "")
			{
				foreach ($data["TeamOne"]["players"] as $player) 
				{
					$data["error"] = $this->StatModel->validate_entry($player["playerStats"]);
					if($data["error"] != ""){
						break;
					}else {
						$Total = 2 * $player["playerStats"]["twoPoints"] + 3 * $player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];
					}
					$GTOTAL += $Total;
				}
			}


			if($data["error"] == ""){
				foreach ($data["TeamTwo"]["players"] as $player) 
				{
					$data["error"] = $this->StatModel->validate_entry($player["playerStats"]);
					if($data["error"] != ""){
						break;
					}else {
						$Total2 = 2 * $player["playerStats"]["twoPoints"] + 3 * $player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];
					
					}
					$ctr++;
					$GTOTAL2 += $Total2;

				}
			}

			if($data["error"] == "")
			{
				if ($GTOTAL != $QTRTOT1 || $GTOTAL2 != $QTRTOT2 )
				{
					$data["error"] = "Point total does not match.";

				}
			}


			if($data["error"] == "")
			{

				foreach ($data["TeamOne"]["players"] as $player) 
				{
					$player["playerStats"]["totalPoints"] = 2 * $player["playerStats"]["twoPoints"] + 3*$player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];

					$player["playerStats"]["playerID"] = $player["idPlayer"];
					$player["playerStats"]["gameNo"] = $gameNo;

					//$this->playerStatModel->update($player["playerStats"]); 
				}




				foreach ($data["TeamTwo"]["players"] as $player)  
				{

					$player["playerStats"]["totalPoints"] = 2 * $player["playerStats"]["twoPoints"] + 3* $player["playerStats"]["threePoints"] + $player["playerStats"]["freeThrows"];

					$player["playerStats"]["playerID"] = $player["idPlayer"];
					$player["playerStats"]["gameNo"] = $gameNo;

					//$this->playerStatModel->update($player["playerStats"]); 

				}


				foreach ($data["TeamOne"]["quarters"] as $quarter) {
				//	$this->QtrModel->update($quarter); 	 

				}



				foreach ($data["TeamTwo"]["quarters"] as $quarter) 
				{
				//	$this->QtrModel->update($quarter); 		

				}

				$data["update"] = "success";
			}
		}
		else
		{
			$data["TeamOne"]["team"] = $this->teamModel->get($game[0]["teamA"])->result_array()[0];
			$data["TeamOne"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamA"])->result_array();
			
			$i = 0;

			foreach ($data["TeamOne"]["players"] as $player) 
			{
				$data["TeamOne"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
				$i++;
			}

			$data["TeamOne"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamOne"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamA[0]["overTimeQuarterNo"])->result_array()[0];

			$data["TeamTwo"]["team"] = $this->teamModel->get($game[0]["teamB"])->result_array()[0];
			$data["TeamTwo"]["players"] = $this->playerModel->get_players_at_teamID($game[0]["teamB"])->result_array();
			$i = 0;
			foreach ($data["TeamTwo"]["players"] as $player) 
			{
				$data["TeamTwo"]["players"][$i]["playerStats"] = $this->playerStatModel->get($player["idPlayer"],$gameNo)->result_array()[0];
				$i++;
			}
			$data["TeamTwo"]["quarters"]["firstQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["firstQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["secondQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["secondQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["thirdQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["thirdQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["fourthQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["fourthQuarterNo"])->result_array()[0];
			$data["TeamTwo"]["quarters"]["overTimeQuarter"] = $this->QtrModel->get($gameStatTeamB[0]["overTimeQuarterNo"])->result_array()[0];
			$data["gameNo"] = $gameNo;
		}
		
		$data["gameDetail"] = $game; 
		$this->load->view('statInput', $data);

	}

	
}
?>
