<?php 
class Game extends CI_Model {

	public function __construct()
	{
		parent::__construct();
        $this->load->helper('url');
	}

	public function insert($game)
	{
		$data = array(
			'date' => $game["date"],
			'start' => $game["start"],
			'end' => $game["end"],
			'teamA' => $game["teamA"],
			'teamB' => $game["teamB"],
			'court' => $game["court"]
		);
		$this->db->insert('game', $data); 
		return $this->db->insert_id();
	}
	public function update($game)
	{
		$data = array(
			'date' => $game["date"],
			'start' => $game["start"],
			'end' => $game["end"],
			'teamA' => $game["teamA"],
			'teamB' => $game["teamB"],
			'court' => $game["court"]
		);
		$this->db->where("start", $game["start"]);
		$this->db->where("date", $game["date"]);
		$this->db->where("court", $game["court"]);
	
		$this->db->update('game', $data);
		
	}

	public function get($id){
		$this->db->select('*');
		$this->db->where('idGame', $id);
		$query = $this->db->get('game');
		return $query;
	}
	public function getWhereDate($date){
		$this->db->select('*');
		$this->db->where('date', $date);
		$this->db->order_by("idGame", "asc");
		$query = $this->db->get('game');
		return $query;
	}

	public function deleteWhereDate($date){

		$data = array(
			'date' => $date
		);
		$this->db->delete('quarterstatistics', $data); 
		return $this->db->affected_rows();
	}
} 
?>