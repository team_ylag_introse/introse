<html>

	<head>
		<title><?php echo $title; ?></title>
		
		<!-- Bootstrap theme -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme.css');?>">

		<!-- Bootstrap theme for non admin pages -->
		<link rel="stylesheet" type="text/css" href="<?php  echo base_url('resources/css/theme-fornotadmin.css');?>">

		<!-- Logo on top of page -->
		<link rel="icon" type="image/ico" href="<?php  echo base_url('resources/images/logo.png');?>" />

		<!-- JQUERY -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/jquery.js')?>"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/bootstrap.js')?>"></script>

		<!-- Registration JS -->
		<script type="text/javascript" src="<?php  echo base_url('resources/js/registration.js')?>"></script>		

	</head>


	<body>
	<!-- Navbar for Public pages -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container-fluid">
		    	<div class="navbar-header">
		      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
		      		</button>
		      		
		      		<!-- LSAL LOGO HERE --> 
		      		<a class="navbar-brand" href="#">
		      			<img class="navbar-logo" alt="LSSC-Logo" src="<?php  echo base_url('resources/images/logo.png');?>" height="50px"/>
		      		</a>
		    	</div>

		    	<!-- Use "li class='active' if page is in a correct tab -->
		    	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      		<ul class="nav navbar-nav">
		      			<li><a href="<?php echo base_url('/Home');?>">Home</a></li>
				        <li ><a href="<?php echo base_url('/home/viewmatch');?>">Schedule <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo base_url('/home/viewbracket');?>">Teams & Brackets</a></li>
				   
				        <!--li class="dropdown">
		          			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Something else here</a></li>
								<li class="divider"></li>
								<li><a href="#">Separated link</a></li>
								<li class="divider"></li>
								<li><a href="#">One more separated link</a></li>
							</ul>
		        		</li-->
		      		</ul>
		      		<ul class="nav navbar-nav navbar-right">
						<li id="register"><a href="<?php echo base_url('/registration');?>" id="register-a">Register for LSAL Basketball</a></li>
					</ul>
		      		
		    	</div>
		  	</div>
		</nav>	<!-- end of .nav -->

	<!-- Page contents --> 
		<div class="container" >
			<!-- need po yung page-header div -->
			<div class="page-header" id="banner">
				
				<div class="row">
					<h1><span class="glyphicon glyphicon-pencil"></span> &nbsp;Register your Team</h1>
				</div>

				<br />
				<br />
				<br />
				<?php 
					if (!empty($error))
					{
						echo 	"<div class='panel panel-danger'>
  									<div class='panel-heading'>
    									<h3 class='panel-title'><span class='glyphicon glyphicon-remove'></span>&nbsp;Error</h3>
  									</div>
  									<div class='panel-body'>";
    					echo $error;
  						echo 		"</div>
								</div>";
						
					}

				 ?>
				<form method="POST" action="<?php echo base_url('/registration/register')?>">
					<div class="row">
						<h3> Team Information </h3>

					</div>

					<!-- Team Name field -->
					<div class="row"> 
						<div class="form-group col-md-6 floating-placeholder">
							<label for = "teamname">Team Name</label>
							<input type="text" name ="teamName" class="form-control" id="teamname" placeholder="Team Name" value="<?php echo htmlentities($teamName); ?>">
						</div>
					</div>

					<!-- Number of Players select -->
					<div class="row"> 
						<div class="form-group col-md-6 floating-placeholder">
							<label for = "numPlayers">Number of Players</label>
							<select name ="numPlayers" class="form-control" id="numPlayers" >

							
								<option <?php if($numPlayers == 12) echo "selected"; ?>>12</option>
								<option <?php if($numPlayers == 13) echo "selected"; ?>>13</option>
								<option <?php if($numPlayers == 14) echo "selected"; ?>>14</option>
								<option <?php if($numPlayers == 15) echo "selected"; ?>>15</option>
							
							</select>
						</div>
					</div>

					<br>

					<div class="row">
						<h3>Players Information </h3>
						<h5> Note: Captain should be in the first row</h5>
					</div>

					<!-- Players Table --> 
					<div class="row">
						<table class="table table-bordered table-condensed table-striped table-hover table-register">
      						<thead>
      							<tr>
      								<th>Last Name</th>
      								<th>First Name</th>
      								<th>Middle Name</th>
      								<th>ID. No </th>
      								<th>Contact No.</th>
      								<th>Uniform # (1-99)</th>
      								<th>Uniform Size</th>
      							</tr>

      						</thead>
<!-- ROWS ARE MADE THROUGH JS/JQUERY. Please refer to Registration.js -->


      						<tbody >
      							<?php 
      							
  								for ($i=0; $i < $numPlayers; $i++) 
  								{ 
      							?>
						         <tr class="">
						         	<td><input type="text" class="form-control" value="<?php echo htmlentities($player[$i]["lastName"]); ?>" placeholder='<?php if($i == 0) echo 'Captain'; else echo "Player #[".($i+1)."]"; ?>' name="player[<?php echo $i; ?>][lastName]"/></td> 
									<td><input type="text" class="form-control" value="<?php echo htmlentities($player[$i]["firstName"]); ?>" name="player[<?php echo $i; ?>][firstName]"/></td>
									<td><input type="text" class="form-control" value="<?php echo htmlentities($player[$i]["middleName"]); ?>" name="player[<?php echo $i; ?>][middleName]"/></td>
									<td style="width: 140px"><input type="text" class="form-control" value="<?php echo htmlentities($player[$i]["idNo"]); ?>" name="player[<?php echo $i; ?>][idNo]"/></td>
									<td><input type="text" class="form-control" value="<?php echo htmlentities($player[$i]["contactNo"]); ?>" name="player[<?php echo $i; ?>][contactNo]"/></td>
									<td style="width: 145px;"><input type="text" class="form-control" value="<?php echo htmlentities($player[$i]["uniformNo"]); ?>" name="player[<?php echo $i; ?>][uniformNo]"/></td>
									<td>
										<select class="form-control" name="player[<?php echo $i; ?>][uniformSize]">
											<option disabled selected> -- Shirt Size -- </option>
											<option <?php if($player[$i]['uniformSize'] == "XS") echo "selected"; ?> >XS</option>
											<option <?php if($player[$i]['uniformSize'] == "S") echo "selected"; ?> >S</option>
											<option <?php if($player[$i]['uniformSize'] == "M") echo "selected"; ?> >M</option>
											<option <?php if($player[$i]['uniformSize'] == "L") echo "selected"; ?> >L</option>
											<option <?php if($player[$i]['uniformSize'] == "XL") echo "selected"; ?> >XL</option>
											<option <?php if($player[$i]['uniformSize'] == "XXL") echo "selected"; ?> >XXL</option>
										</select>
									</td>
						        </tr>
						        <?php 
						        } 
						        ?>
      						</tbody>
    					</table>
					</div>
					<!-- End of Players table-->

					<br> <br> <br>
					<div  class="row">
						<center>
							<div class="col-md-6 col-md-offset-1">
								<p>
									Please double check the following information provided.
									Please don't forget to pay the registration fee which is <b>P 1,300 </b> per player. 
									<br><br>
									<b>Incomplete payment/no payment at all will invalidate your team's registration </b>

								</p>
							</div>

							<div class="form-group">
								<br>
								<input type="submit" class="btn btn-success btn-lg"></input>

							</div>
						</center>

					</div>
					
				</form> 
			</div>
		</div>

	</body>

	


</html>